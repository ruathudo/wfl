<?php
//var $helloworld;
class WPFB_DownloadEntry {
	var $download_id;
	var $file_id;
	var $download_ip;
	var $download_time;
	var $cat_name;
}
class WPFB_GraphData {
	var $download_entries;
	
	// Constructor:
	function WPFB_GraphData() {
		$this->load();
	}
	
	function load() {
		global $wpdb;
		$rows = $wpdb->get_results("SELECT * FROM " . $wpdb->wpfilebase_downloads);
		foreach ($rows as $row) {
			$download_entry = new WPFB_DownloadEntry();
			$download_entry->download_id = $row->download_id;
			$download_entry->file_id = $row->file_download_id;
			$download_entry->download_ip = $row->download_ip;
			$download_entry->download_time = $row->download_time;
			$cat = $wpdb->get_row("SELECT `file_category_name` FROM $wpdb->wpfilebase_files WHERE file_id = " . $row->file_download_id );
			$download_entry->cat_name = $cat->file_category_name;
			$this->download_entries[] = $download_entry;
		}
	}
}

?>