jQuery(document).ready( function($) {

    $( '.avp-meta-box-control' ).click(function(){
        switch_tabs($(this));
    });
 
});
 
function switch_tabs(obj)
{
    //  Get the ID from the passed object and the 'rel' identifier for it
    var id = '#' + obj.attr( 'rel' );
	var objclass = '.' + jQuery(id).attr('class');
    //  Show the associated contents panel with the ID that matches the object 'rel' identifier
	
	//console.log(objclass);
	jQuery(objclass).hide();
    jQuery( id ).toggle( );
};
