<?php
/*
Plugin Name: Journal Navigation Widget
Plugin URI: http://
Description: Journal Navigation Widget
Author: Cuong Le
Version: 1
Author URI: http://
*/
 
 
class JournalNavigationWidget extends WP_Widget
{
  function JournalNavigationWidget()
  {
    $widget_ops = array('classname' => 'JournalNavigationWidget', 'description' => '' );
    $this->WP_Widget('JournalNavigationWidget', 'Journal Navigation Widget', $widget_ops);
	//wp_enqueue_script('jquery');
	//wp_register_script('sub-cat-display', '/wp-content/plugins/journal-navigation/subcatdisplay.js', false, '' );
	//wp_enqueue_script('sub-cat-display');
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '','cat_id' => '' ) );
    $title = $instance['title'];
    $category = $instance['category'];
    $display_parent = $instance['display_parent'];

	   // Get the existing categories and build a simple select dropdown for the user.
		$categories = get_categories(array( 'hide_empty' => 0));
 
		$cat_options = array();
		$cat_options[] = '<option value="BLANK">Select one...</option>';
		foreach ($categories as $cat) {
			$selected = $category === $cat->cat_ID ? ' selected="selected"' : '';
			$cat_options[] = '<option value="' . $cat->cat_ID .'"' . $selected . '>' . $cat->name . '</option>';
		}
?>
  <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
  <!--<p><label for="<?php echo $this->get_field_id('cat_id'); ?>">Category id: <input class="widefat" id="<?php echo $this->get_field_id('cat_id'); ?>" name="<?php echo $this->get_field_name('cat_id'); ?>" type="text" value="<?php echo attribute_escape($cat_id); ?>" /></label></p>-->

<p>
	<label for="<?php echo $this->get_field_id('category'); ?>">
		<?php _e('Choose category (optional):'); ?>
	</label>
	<select id="<?php echo $this->get_field_id('category'); ?>" class="widefat" name="<?php echo $this->get_field_name('category'); ?>">
		<?php echo implode('', $cat_options); ?>
	</select>
</p>
  <?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
	$instance['category'] = $new_instance['category'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
    echo $before_widget;
    $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
 
    if (!empty($title))
      echo $before_title . $title . $after_title;;
 
    // WIDGET CODE GOES HERE
	$cat_id = $instance['category'];
	//echo $cat_id;
	$subcategories = get_categories(array('parent' => $cat_id, 'hide_empty' => false));
	//echo implode(",",$subcategories);
	//echo $subcategories;
	$i=0;
	$output='';
	$year_div='';
	$issue_link='';
	$issue_div='';
	
	$output.='<div class="container-journal-nav-widget">';
	
	usort($subcategories, "compareCategoryName"); // sorting function "compareCategoryName" is stored in functions.php of theme
	
	foreach ( $subcategories as $subcat ) {
		//echo $subcat->cat_ID;
		if ( $i % 4 == 0 && $i != 0) {
			$output .= '<div class="row-journal-nav-widget">'.'<div class="sub-cat">'.$year_div.'</div>'.$issue_div.'</div>';
			$issue_div='';
			$year_div='';
		}
		$year_div .= '<a class="journal-year-link" href="'.get_category_link($subcat->cat_ID).'">'.$subcat->cat_name.'</a>';
		
		// $subsubcategories = get_categories(array('parent' => $subcat->cat_ID, 'hide_empty' => false));
		// $issue_link = '<div class="sub-sub-cat" >';
		// foreach ($subsubcategories as $subsubcat) {
			// $issue_link .= '<a class="issue-link" href="'.get_category_link($subsubcat->cat_ID).'">'. $subsubcat->cat_name.'</a>';
		// }
		// $issue_link .= '</div>';
		// $issue_div .=$issue_link;
		$i++;
	}
	$output .= '<div class="row-journal-nav-widget">'.'<div class="sub-cat">'.$year_div.'</div>'.$issue_div.'</div>';
	$output.= '</div>';
	//$output.= '<style>.journal-year-link {margin: 0px 5px;} .issue-link {margin: 0px 3px;} .sub-sub-cat {margin: 0px 2px;}</style>';
	echo $output;

    echo $after_widget;
  }
 
}
add_action( 'widgets_init', create_function('', 'return register_widget("JournalNavigationWidget");') );?>