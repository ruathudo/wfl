<?php
/*
Plugin Name: Rich Tax Description Editor
Plugin URI: http://infinity-8.me
Description: Turns Description field on Taxonomy pages into the full scale Rich Text Editor.
Version: 1.0
Author: Davit Barbakadze
Author URI: http://infinity-8.me
*/

require_once(dirname(__FILE__) . '/i8/class.Plugino.php');

class RTE extends RTE_Plugino {	
	
	function __construct()
	{
		parent::__construct(__FILE__);
		
		if (version_compare(get_bloginfo('version'), '3.3', '<') || !function_exists('wp_editor')) {
			$this->warn("<strong>{$this->info['Name']}</strong> requires <strong><i>wp_editor API</i></strong>, which seems to be not available in <i>your version</i> of WordPress. Make sure that you are running at least <strong><i>version 3.3</i></strong>.<br /><br /> Plugin will <strong><i>deactivate</i></strong> itself now!");	
		}
	}
	
	function a__init()
	{
		foreach (get_taxonomies() as $tax) {
			add_action("{$tax}_pre_add_form", create_function('', 'ob_start();'));
			add_action("{$tax}_pre_edit_form", create_function('', 'ob_start();'));
			add_action("{$tax}_add_form", array($this, 'a_term_edit_form'), 10, 1);
			add_action("{$tax}_edit_form", array($this, 'a_term_edit_form'), 10, 2);
		}	
	}
	
	function a_term_edit_form()
	{
		$html = ob_get_clean();
		
		$args = func_get_args();
		$content = sizeof($args) == 2 ? htmlspecialchars_decode($args[0]->description) : '';
		
		ob_start();
		
		$args = array(
			'textarea_name' => 'description', // id cannot be 'description', 'cause this causes UI conflict on a list page
			'editor_css' => '<style>.quicktags-toolbar input { width: auto; } .form-field textarea#desc { border: none; }</style>' // fix some defalt styles
		);
		
		 // load minimal version on list page	
		if (!isset($_GET['action']) || $_GET['action'] != 'edit') {
			$args += array(
				'teeny' => true,
				'textarea_rows' => 6,
				'tinymce' => array(
					'theme_advanced_buttons1' => 'bold, italic, underline, blockquote, separator, strikethrough, undo, redo, justifyleft, justifycenter, justifyright, link, unlink, fullscreen',
					'theme_advanced_buttons2' => 'formatselect, forecolor, backcolor, bullist, numlist, outdent, indent, separator, removeformat'
				)
			);
		}
		
		wp_editor($content, 'desc', $args);
		echo preg_replace('|<textarea name="description"[\s\S]+?</textarea>|', ob_get_clean(), $html);
	}
	
	// disable extra sanitization on description field
	function f_0__pre_term_description($description)
	{
		remove_filter('pre_term_description', 'wp_filter_kses'); // disable kses on term description
		return $description;	
	}
}

new RTE;
