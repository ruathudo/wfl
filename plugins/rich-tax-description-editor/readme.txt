=== Rich Tax Description Editor ===
Contributors: jayarjo
Donate link: http://infinity-8.me
Tags: taxonomy, rich text editor
Requires at least: 3.3
Tested up to: 3.3.1
Stable tag: 1.0

Turns boring Description field on Taxonomy pages into full scale Rich Text Editor.

== Description ==

No Settings, no hassle. Clean and short. Activate it and you will get pretty, handy Rich Text Editor on all Taxonomy pages, including Tags, Categories and any other taxonomies you will ever create in future.

== Installation ==

1. Go to: Plugins > Add New > upload
2. Browse for `rich-tax-description-editor-x.x.x.zip` file
3. Hit `Install Now`
4. Activate

or

1. Upload contents of `rich-tax-description-editor-x.x.x.zip` to `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==


== Screenshots ==

1. 
2. 

== Changelog ==

= 1.0 = 
Initial release.
