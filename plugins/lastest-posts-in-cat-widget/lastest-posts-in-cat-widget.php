<?php
/*
Plugin Name: Lastest Posts in Category Widget
Plugin URI: http://
Description: Display in a widget the lastest post in a selected category
Author: Cuong Le
Version: 1
Author URI: http://
*/
 
 
class LastestPostsWidget extends WP_Widget
{
  function LastestPostsWidget()
  {
    $widget_ops = array('classname' => 'LastestPostsWidget', 'description' => '' );
    $this->WP_Widget('LastestPostsWidget', 'Lastest posts Widget', $widget_ops);
	wp_register_style( 'lastest-post-cat-widget-style', plugins_url('lastest-posts-widget-style.css', __FILE__) );
    wp_enqueue_style( 'lastest-post-cat-widget-style' );
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '') );
    $title = $instance['title'];
    $category = $instance['category'];
	$num_post = $instance['num_post'];
	$read_more_text = $instance['read_more_text'];
	
	// Get the existing categories and build a simple select dropdown for the user.
	$categories = get_categories(array( 'hide_empty' => 0));

	$cat_options = array();
	$cat_options[] = '<option value="BLANK">Select one...</option>';
	foreach ($categories as $cat) {
		$selected = $category === $cat->cat_ID ? ' selected="selected"' : '';
		$cat_options[] = '<option value="' . $cat->cat_ID .'"' . $selected . '>' . $cat->name . '</option>';
	}
?>
	<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
	<p>
	<label for="<?php echo $this->get_field_id('category'); ?>">
		<?php _e('Choose category (optional):'); ?>
	</label>
	<select id="<?php echo $this->get_field_id('category'); ?>" class="widefat" name="<?php echo $this->get_field_name('category'); ?>">
		<?php echo implode('', $cat_options); ?>
	</select>
	</p>
	<p><label for="<?php echo $this->get_field_id('num_post'); ?>"><?php _e('Number of posts:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('num_post'); ?>" name="<?php echo $this->get_field_name('num_post'); ?>" type="text" value="<?php echo attribute_escape($num_post); ?>" /></label></p>
	<p><label for="<?php echo $this->get_field_id('read_more_text'); ?>"><?php _e('Read-more Text:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('read_more_text'); ?>" name="<?php echo $this->get_field_name('read_more_text'); ?>" type="text" value="<?php echo attribute_escape($read_more_text); ?>" /></label></p>

  <?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
	$instance['category'] = $new_instance['category'];
	$instance['num_post'] = $new_instance['num_post'];
	$instance['read_more_text'] = $new_instance['read_more_text'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
	?>
	<div class="lastest-posts-cat-wrapper">
		<?
		extract($args, EXTR_SKIP);
		echo $before_widget;
		$title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
	 
		if (!empty($title))
		  echo $before_title . $title . $after_title;;
	 
		// WIDGET CODE GOES HERE
		$latest_cat_post = new WP_Query( array('posts_per_page' =>$instance['num_post'], 'category__in' => $instance['category']));
		//var_dump($latest_cat_post);
		while ( $latest_cat_post->have_posts() ) :
			$latest_cat_post->the_post();
			echo '<p class="entry-announcement-title"><a href="' .get_permalink() .'">' . get_the_title() . '</a></p>';
		endwhile;

		/* Restore original Post Data */
		wp_reset_postdata();
		
		echo '<p class="read-more-text"><a href="'. get_category_link($instance['category']) .'">' . $instance['read_more_text'] .  '</a></p>';
		echo $after_widget;
		?>
	</div>
	<?php
  }
  
	// function prefix_add_my_stylesheet() {
        // // Respects SSL, Style.css is relative to the current file
        // wp_register_style( 'lastest-post-cat-widget-style', plugins_url('style.css', __FILE__) );
        // wp_enqueue_style( 'lastest-post-cat-widget-style' );
    // }
 
}
add_action( 'widgets_init', create_function('', 'return register_widget("LastestPostsWidget");') );?>