<?php
/*
Plugin Name: Statistics Widget
Plugin URI: http://
Description: Statistics Widget make a display of statistics
Author: Cuong Le
Version: 1
Author URI: http://
*/
 
 
class StatisticsWidget extends WP_Widget
{
  function StatisticsWidget()
  {
    $widget_ops = array('classname' => 'StatisticsWidget', 'description' => 'Displays an graph of statistics' );
    $this->WP_Widget('StatisticsWidget', 'Statistics Widget', $widget_ops);
	//wp_deregister_script('jquery');
	//wp_register_script( 'jquery', 'http://code.jquery.com/jquery-latest.pack.js', false, '' );
	wp_enqueue_script('jquery');
	wp_register_script( 'jQueryflot', '/wp-content/plugins/statistics/js/jquery.flot.min.js', false, '' );
	wp_enqueue_script('jQueryflot');
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
    $title = $instance['title'];
?>
  <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
 
    echo $before_widget;
    $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
 
    if (!empty($title))
      echo $before_title . $title . $after_title;;
 
    // WIDGET CODE GOES HERE
	$testObj = new CountPerDay();
	
    echo "<div class=\"statistics-widget\">";
	global $count_per_day;
	include_once('wp-content\plugins\count-per-day\counter.php');
	//if(method_exists($count_per_day,"show")) echo $count_per_day->getReadsAll(true);
	
	$testObj->getFlotChartWidget();
	$testObj->getUserPerMonth(false,false);
	echo"</div>";
 
    echo $after_widget;
  }
 
}
add_action( 'widgets_init', create_function('', 'return register_widget("StatisticsWidget");') );?>