<?php
/**
 * The template for displaying Category pages.
 *
 * Used to display archive-type pages for posts in a category.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */

get_header(); ?>
	<div id="thirdary" class="left-side clearfix" role="complementary">
			<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</div><!-- #secondary -->
	<section id="primary" class="site-content">
		<div id="content" role="main" class="clearfix">

		 <!-- display 6 post in category book -->

		<?php if ( have_posts() ) : ?>
		
			<div class="archive-header">

			
            
			</div><!-- .archive-header -->
			<?php
			
			//query_posts( array ( 'category_name' => 'books', 'posts_per_page' => 3 ) );
			while ( have_posts() )
			{
				the_post();
				get_template_part( 'content', 'book' );
			}
			wfl_theme_content_nav( 'nav-below' );
			
			?>
			<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta clearfix"><?php echo category_description(); ?></div>
			<?php endif; ?>
			
			<?php else : ?>
                <?php get_template_part( 'content', 'none' ); ?>
            <?php endif; ?>

            
		</div><!-- #content -->
        
	</section><!-- #primary -->

	<!--  
<script type="text/javascript"> 
        		
        		function short(){

        			var length = 50;
        			var book_title = document.getElementsByClassName('book-title');
        			

        			for(var i=0; i<book_title.length; i++)
        			{
        				if(book_title[i].text.length > length){
        					var trimmed = book_title[i].text.substring(0,length);
        					book_title[i].innerHTML = trimmed + "&hellip;";
        				}
        				
        			}

        		}

      			short();
      			// add class clearfix and display-book class to article to wrap book details
      			/*
      			$(document).ready(function() {
      				$("article").addClass("clearfix display-book");
      			}); */
</script>
-->
<?php get_sidebar(); ?>
<?php get_footer(); ?>