<?php
/**
 * The template for displaying Category pages.
 *
 * Used to display archive-type pages for posts in a category.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */

get_header(); ?>
	<div id="thirdary" class="widget-area-left" role="complementary">
			<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</div><!-- #secondary -->
	<section id="primary" class="site-content">
		<div id="content" role="main">
		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<!--<h1 class="archive-title"><?php //printf( __( 'Category Archives: %s', 'wfl_theme' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>-->

			<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php echo category_description(); ?></div>			
            <?php endif; ?>
			</header><!-- .archive-header -->

			<script>
				function showSection(foo){
					//alert(foo);
					// change color when anchors r clicked
				/*	var anchors = document.getElementsByClassName("anchor");
					for ( i = 0; i < anchors.length; i++) {
						anchors[i].style.color = "blue";
					} */

					var issue_articles = document.getElementsByTagName("issue-article");
					for ( i = 0; i < issue_articles.length; i++) {
						if (foo == "All") {
							issue_articles[i].className = "clearfix";
						}
						else {
							issue_articles[i].className = "div_no_display";
						}

					}

					document.getElementById(foo + "_anchor").style.color = "red";	
					if (foo != "All")
						document.getElementById(foo + "_post").className = "clearfix";						
					
				}
			</script>
			
			<a href="#" class="scrollup" style="display: inline;">Scroll</a> <!-- scollup botton -->
			<hr>
			<div class='alphalist'>
				<ul class='hlist'>
			<?php $filter_args = array(
					'posts_per_page'  => -1,
				    'offset'          => 0,
				    'category'        => get_cat_ID('Authors'),
				    'orderby'         => 'post_title',
				    'order'           => 'ASC',
				    'include'         => '',
				    'exclude'         => '',
				    'meta_key'        => '',
				    'meta_value'      => '',
				    'post_type'       => 'post',
				    'post_mime_type'  => '',
				    'post_parent'     => '',
				    'post_status'     => 'publish',
				    'suppress_filters' => true );

				$posts = get_posts( $filter_args );

				foreach ($posts as $post) :
					$title = $post->post_title;
					echo "<li><a href='#".$title."' id='".$title."_anchor' onclick=\"showSection('".$title."')\" class='anchor'>".$title."</a></li>";
				endforeach;
				echo "<li><a href='#All' id='All_anchor' onclick=\"showSection('All')\" class='anchor'>All</a></li>";
				echo "</ul></div>";
			?>		
		
			<?php 
				$category = get_the_category();
				$args = 'cat=' . $category[0]->cat_ID . '&showposts=-1'. '&order=ASC';
				//var_dump($args);
				query_posts( $args );
				while ( have_posts() ) 
				{
					the_post();
					get_template_part( 'content', 'author' );
				}
				//wfl_theme_content_nav( 'nav-below' );
				wp_reset_query();
			?>

			<?php else : ?>
            	<?php get_template_part( 'content', 'none' ); ?>
            <?php endif; ?>
			<script>
				showSection('All');
			</script>
		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>