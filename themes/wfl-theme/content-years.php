<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */
$category = get_category( get_query_var( 'cat' ) );
$cat_id = $category->cat_ID;
// start journal-view
// list of years will be displayed here ( EX: archive - 2013 - 2012 - 2011 - ... )
$args = array(
'type'                     => 'post',
'child_of'                 => $cat_id,
'parent'                   => $cat_id,
'orderby'                  => 'name',
'order'                    => 'ASC',
'hide_empty'               => 0,
'hierarchical'             => 1,
'exclude'                  => '',
'include'                  => '',
'number'                   => '',
'taxonomy'                 => 'category',
'pad_counts'               => false );
$categories = get_categories( $args ); 
//print_r($categories); 
?>
<div>
	<?php
	
	if ( $categories[0] && strlen($categories[0]->cat_name) ==4)
		usort($categories, "compareCategoryName");
	
	
	$i=0;
	echo '<div class="journal-year-container">';
		
		echo '<p>Archive</p>';
		echo '<div class="journal-year-item clearfix">';

		foreach ($categories as $category) {
			
			echo '<a href="'.get_category_link( $category->cat_ID ).'">';
			echo $category->cat_name . '</a>';
			
		}
		echo '</div>';
		include (ABSPATH . '/wp-content/plugins/responsive-logo-slideshow/responsive-logo-slider.php');
		
	echo '</div>'; /* journal container*/

?>
	<div class="impact-factor"><?php echo apply_filters( 'the_content','[do action="impact-factor"/]'); ?></div>
</div>
<?php
if ( category_description() || true ) : // Show an optional category description ?>
	<div class="archive-meta"><?php echo category_description(); ?></div>
<?php endif; ?>


