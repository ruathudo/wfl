<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="listed-item-entry-header">
			<p class="listed-item-entry-title">
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'wfl_theme' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
			</p>
            <!--Viewed:--> <?php // echo do_shortcode('[post_view]'); ?>
		</header><!-- .entry-header -->
	</article><!-- #post -->
