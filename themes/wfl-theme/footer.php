<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */
?>


	</div><!-- #main .wrapper -->
    


</div><!-- #page -->
    <footer id="site-footer" class="footer-wrapper clearfix">

		
			<div class="bottomMenu clearfix"><!-- #menu -->
              <?php wp_nav_menu( array( 'theme_location' => 'third', 'menu_class' => 'bottomMenu' ) ); ?>  
			</div><!-- #menu -->
			
			<div id="social-icon">
			<ul>
				<li>
					<a target="_blank" href="https://www.facebook.com/pages/WFL-Publisher/161179813932344">
						<span id="facebook"></span>find us on facebook
					</a>
				</li>
				<li>
					<a target="_blank" href="http://fi.linkedin.com/pub/wfl-publisher/2b/131/83a">
						<span id="in"></span>connect us on linkedin
					</a>
				</li>
				<li>
					<a target="_blank" href="https://twitter.com/wflpublisher">
						<span id="twitter"></span>follow us on twitter
					</a>
				</li>
			</ul>

			</div>


			<div id="footer-contact"> <!-- contact -->
				<p align="center">
					
					<span>
						<?php
						echo get_reusable_part('Site footer', '<br/>','');
						?>
					</span>
				</p>
			</div> <!-- contact -->


    </footer> <!-- /#footer -->


<?php wp_footer(); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37557993-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

// full height for view port


$(document).ready(function() {
	var docHeight = $(window).height();
	var footerHeight = $('#site-footer').outerHeight();
	var footerTop = $('#site-footer').position().top + footerHeight;
   	if (footerTop < docHeight) {
    $('#site-footer').css('margin-top',(docHeight - footerTop)  +'px'); 
	}	
});


/* end function full height */

</script>

</body>
</html>