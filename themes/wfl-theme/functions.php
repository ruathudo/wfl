<?php
/**
 * Wfl Theme functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used
 * in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook.
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */

/**
 * Sets up the content width value based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 625;

/**
 * Sets up theme defaults and registers the various WordPress features that
 * Wfl Theme supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails, automatic feed links,
 * 	custom background, and post formats.
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Wfl Theme 1.0
 */
function wfl_theme_setup() {
	/*
	 * Makes Wfl Theme available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Wfl Theme, use a find and replace
	 * to change 'wfl_theme' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'wfl_theme', get_template_directory() . '/languages' );

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// This theme supports a variety of post formats.
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );

	// This theme uses wp_nav_menu() in two location.
	register_nav_menu( 'primary', __( 'Primary Menu', 'wfl_theme' ) );
	register_nav_menu( 'secondary', __( 'Secondary Menu', 'wfl_theme' ) );
	register_nav_menu( 'third', __( 'Third Menu', 'wfl_theme' ) );  // rua modified
	/*
	 * This theme supports custom background color and image, and here
	 * we also set up the default background color.
	 */
	add_theme_support( 'custom-background', array(
		'default-color' => 'e6e6e6',
	) );

	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop
}
add_action( 'after_setup_theme', 'wfl_theme_setup' );


/**
 * Adds support for a custom header image.
 */
require( get_template_directory() . '/inc/custom-header.php' );

/**
 * Enqueues scripts and styles for front-end.
 *
 * @since Wfl Theme 1.0
 */
function wfl_theme_scripts_styles() {
	global $wp_styles;

	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/*
	 * Adds JavaScript for handling the navigation menu hide-and-show behavior.
	 */
	wp_enqueue_script( 'wfl_theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '1.0', true );

	/*
	 * Loads our special font CSS file.
	 *
	 * The use of Open Sans by default is localized. For languages that use
	 * characters not supported by the font, the font can be disabled.
	 *
	 * To disable in a child theme, use wp_dequeue_style()
	 * function mytheme_dequeue_fonts() {
	 *     wp_dequeue_style( 'wfl_theme-fonts' );
	 * }
	 * add_action( 'wp_enqueue_scripts', 'mytheme_dequeue_fonts', 11 );
	 */

	/* translators: If there are characters in your language that are not supported
	   by Open Sans, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Open Sans font: on or off', 'wfl_theme' ) ) {
		$subsets = 'latin,latin-ext';

		/* translators: To add an additional Open Sans character subset specific to your language, translate
		   this to 'greek', 'cyrillic' or 'vietnamese'. Do not translate into your own language. */
		$subset = _x( 'no-subset', 'Open Sans font: add new subset (greek, cyrillic, vietnamese)', 'wfl_theme' );

		if ( 'cyrillic' == $subset )
			$subsets .= ',cyrillic,cyrillic-ext';
		elseif ( 'greek' == $subset )
			$subsets .= ',greek,greek-ext';
		elseif ( 'vietnamese' == $subset )
			$subsets .= ',vietnamese';

		$protocol = is_ssl() ? 'https' : 'http';
		$query_args = array(
			'family' => 'Open+Sans:400italic,700italic,400,700',
			'subset' => $subsets,
		);
		wp_enqueue_style( 'wfl_theme-fonts', add_query_arg( $query_args, "$protocol://fonts.googleapis.com/css" ), array(), null );
	}

	/*
	 * Loads our main stylesheet.
	 */
	wp_enqueue_style( 'wfl_theme-style', get_stylesheet_uri() );

	/*
	 * Loads the Internet Explorer specific stylesheet.
	 */
	wp_enqueue_style( 'wfl_theme-ie', get_template_directory_uri() . '/css/ie.css', array( 'wfl_theme-style' ), '20121010' );
	$wp_styles->add_data( 'wfl_theme-ie', 'conditional', 'lt IE 9' );
}
add_action( 'wp_enqueue_scripts', 'wfl_theme_scripts_styles' );

/**
 * Creates a nicely formatted and more specific title element text
 * for output in head of document, based on current view.
 *
 * @since Wfl Theme 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string Filtered title.
 */
function wfl_theme_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'wfl_theme' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'wfl_theme_wp_title', 10, 2 );

/**
 * Makes our wp_nav_menu() fallback -- wp_page_menu() -- show a home link.
 *
 * @since Wfl Theme 1.0
 */
function wfl_theme_page_menu_args( $args ) {
	if ( ! isset( $args['show_home'] ) )
		$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'wfl_theme_page_menu_args' );

/**
 * Registers our main widget area and the front page widget areas.
 *
 * @since Wfl Theme 1.0
 */
function wfl_theme_widgets_init() {
	
	register_sidebar( array(
		'name' => __( 'Right Main Sidebar', 'wfl_theme' ),
		'id' => 'sidebar-1',
		'description' => __( 'Appears on posts and pages.', 'wfl_theme' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Left Main Sidebar', 'wfl_theme' ),
		'id' => 'sidebar-2',
		'description' => __( 'Appears on posts and pages.', 'wfl_theme' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Second Front Page Widget Area', 'wfl_theme' ),
		'id' => 'sidebar-3',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'wfl_theme' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
add_action( 'widgets_init', 'wfl_theme_widgets_init' );

if ( ! function_exists( 'wfl_theme_content_nav' ) ) :
/**
 * Displays navigation to next/previous pages when applicable.
 *
 * @since Wfl Theme 1.0
 */
function wfl_theme_content_nav( $html_id ) {
	global $wp_query;

	$html_id = esc_attr( $html_id );

	if ( ($wp_query->max_num_pages > 1) && is_category('books') ) : ?>
		<nav id="<?php echo $html_id; ?>" class="nav-below clearfix" role="navigation">
		<!--<h3 class="assistive-text"><?php _e( 'Post navigation', 'wfl_theme' ); ?></h3> -->
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older books', 'wfl_theme' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer books <span class="meta-nav">&rarr;</span>', 'wfl_theme' ) ); ?></div>
		</nav><!-- #<?php echo $html_id; ?> .navigation -->
	<?php endif;
}
endif;

if ( ! function_exists( 'wfl_theme_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own wfl_theme_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Wfl Theme 1.0
 */
function wfl_theme_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', 'wfl_theme' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'wfl_theme' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<header class="comment-meta comment-author vcard">
				<?php
					echo get_avatar( $comment, 44 );
					printf( '<cite class="fn">%1$s %2$s</cite>',
						get_comment_author_link(),
						// If current post author is also comment author, make it known visually.
						( $comment->user_id === $post->post_author ) ? '<span> ' . __( 'Post author', 'wfl_theme' ) . '</span>' : ''
					);
					printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
						esc_url( get_comment_link( $comment->comment_ID ) ),
						get_comment_time( 'c' ),
						/* translators: 1: date, 2: time */
						sprintf( __( '%1$s at %2$s', 'wfl_theme' ), get_comment_date(), get_comment_time() )
					);
				?>
			</header><!-- .comment-meta -->

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'wfl_theme' ); ?></p>
			<?php endif; ?>

			<section class="comment-content comment">
				<?php comment_text(); ?>
				<?php edit_comment_link( __( 'Edit', 'wfl_theme' ), '<p class="edit-link">', '</p>' ); ?>
			</section><!-- .comment-content -->

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'wfl_theme' ), 'after' => ' <span>&darr;</span>', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</article><!-- #comment-## -->
	<?php
		break;
	endswitch; // end comment_type check
}
endif;

if ( ! function_exists( 'wfl_theme_entry_meta' ) ) :
/**
 * Prints HTML with meta information for current post: categories, tags, permalink, author, and date.
 *
 * Create your own wfl_theme_entry_meta() to override in a child theme.
 *
 * @since Wfl Theme 1.0
 */
function wfl_theme_entry_meta() {
	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'wfl_theme' ) );
	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list('<b>Keyword: </b>', ', ', '');

	$date = sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a>',
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);

	$author = sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( __( 'View all posts by %s', 'wfl_theme' ), get_the_author() ) ),
		get_the_author()
	);

	// Translators: 1 is category, 2 is tag, 3 is the date and 4 is the author's name.
//	if ( $tag_list ) {
//		$utility_text = __( 'This entry was posted in %1$s and tagged %2$s on %3$s<span class="by-author"> by %4$s</span>.', 'wfl_theme' );
//	} elseif ( $categories_list ) {
//		$utility_text = __( 'This entry was posted in %1$s on %3$s<span class="by-author"> by %4$s</span>.', 'wfl_theme' );
//	} else {
//		$utility_text = __( 'This entry was posted on %3$s<span class="by-author"> by %4$s</span>.', 'wfl_theme' );
//	}

	if ( $tag_list ) {
		$utility_text = __( '%2$s', 'wfl_theme' );
	}

	printf(
		$utility_text,
		$categories_list,
		$tag_list,
		$date,
		$author
	);
}
endif;

if ( ! function_exists( 'post_is_in_descendant_category' ) ) {
	function post_is_in_descendant_category( $cats, $_post = null ) {
		foreach ( (array) $cats as $cat ) {
			// get_term_children() accepts integer ID only
			$descendants = get_term_children( (int) $cat, 'category' );
			//var_dump(in_category( $descendants, $_post ));
			if ( $descendants && in_category( $descendants, $_post ) ) {
				
				return true;
			}
		}
		
		return false;
	}
}

if ( ! function_exists( 'category_is_in_descendant_category' ) ) {
	function category_is_in_descendant_category( $cats, $_post = null ) {
		$category = get_category( get_query_var( 'cat' ) );
		$category_id = $category->cat_ID;
		
		//var_dump($category_id);
		foreach ( (array) $cats as $cat ) {
			// get_term_children() accepts integer ID only
			$descendants = get_term_children( (int) $cat, 'category' );
			 // var_dump($descendants);
			//if ( $descendants && in_category( $descendants, $_post ) ) {

			if ( $descendants && in_array($category_id, $descendants) ) {
			
				return true;
			}
		}
		
		return false;
	}
}
/**
 * Extends the default WordPress body class to denote:
 * 1. Using a full-width layout, when no active widgets in the sidebar
 *    or full-width template.
 * 2. Front Page template: thumbnail in use and number of sidebars for
 *    widget areas.
 * 3. White or empty background color to change the layout and spacing.
 * 4. Custom fonts enabled.
 * 5. Single or multiple authors.
 *
 * @since Wfl Theme 1.0
 *
 * @param array Existing class values.
 * @return array Filtered class values.
 */
function wfl_theme_body_class( $classes ) {
	$background_color = get_background_color();

	if ( ! is_active_sidebar( 'sidebar-1' ) || is_page_template( 'page-templates/full-width.php' ) )
		$classes[] = 'full-width';

	if ( is_page_template( 'page-templates/front-page.php' ) ) {
		$classes[] = 'template-front-page';
		if ( has_post_thumbnail() )
			$classes[] = 'has-post-thumbnail';
		if ( is_active_sidebar( 'sidebar-2' ) && is_active_sidebar( 'sidebar-3' ) )
			$classes[] = 'two-sidebars';
	}

	if ( empty( $background_color ) )
		$classes[] = 'custom-background-empty';
	elseif ( in_array( $background_color, array( 'fff', 'ffffff' ) ) )
		$classes[] = 'custom-background-white';

	// Enable custom font class only if the font CSS is queued to load.
	if ( wp_style_is( 'wfl_theme-fonts', 'queue' ) )
		$classes[] = 'custom-font-enabled';

	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	return $classes;
}
add_filter( 'body_class', 'wfl_theme_body_class' );

/**
 * Adjusts content_width value for full-width and single image attachment
 * templates, and when there are no active widgets in the sidebar.
 *
 * @since Wfl Theme 1.0
 */
function wfl_theme_content_width() {
	if ( is_page_template( 'page-templates/full-width.php' ) || is_attachment() || ! is_active_sidebar( 'sidebar-1' ) ) {
		global $content_width;
		$content_width = 960;
	}
}
add_action( 'template_redirect', 'wfl_theme_content_width' );

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @since Wfl Theme 1.0
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 * @return void
 */
function wfl_theme_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
}
add_action( 'customize_register', 'wfl_theme_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 *
 * @since Wfl Theme 1.0
 */
function wfl_theme_customize_preview_js() {
	wp_enqueue_script( 'wfl_theme-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20120827', true );
}
add_action( 'customize_preview_init', 'wfl_theme_customize_preview_js' );

// function compareCategoryName for year comparison
function compareCategoryName( $a, $b) {
	$c = strcmp($a->cat_name,$b->cat_name);
	if ($c == 0) return 0;
		elseif ($c > 0) return -1;
			else return 1;
}

// function comparePostPage for compare 2 custom field name 'page' of 2 posts
function comparePostPage( $a, $b) {
	$pageA = get_post_meta($a->ID, 'page', $single = false);
	$pageB = get_post_meta($b->ID, 'page', $single = false);
	if ( $pageA && $pageB) {
		return intval($pageA[0]) - intval($pageB[0]);
	}
	return -1;
}



// enable shortcodes in excerpts , category descriptions
add_filter('the_excerpt', 'do_shortcode');
add_filter( 'term_description', 'shortcode_unautop');
add_filter( 'term_description', 'do_shortcode' );


// enable tinymce in post title


/* !--beginregion
	functions to correct the behavior of next post / previous post link */
/**
 * Retrieve adjacent post.
 *
 * Can either be next or previous post.
 *
 * @since 2.5.0
 *
 * @param bool $in_same_cat Optional. Whether post should be in a same category.
 * @param array|string $excluded_categories Optional. Array or comma-separated list of excluded category IDs.
 * @param bool $previous Optional. Whether to retrieve previous post.
 * @return mixed Post object if successful. Null if global $post is not set. Empty string if no corresponding post exists.
 */
function get_adjacent_post_same_cat( $in_same_cat = false, $excluded_categories = '', $previous = true ) {
	global $wpdb;

	if ( ! $post = get_post() )
		return null;

	$current_post_date = $post->post_date;
	
	$join = '';
	$posts_in_ex_cats_sql = '';
	if ( $in_same_cat || ! empty( $excluded_categories ) ) {
		$join = " INNER JOIN $wpdb->term_relationships AS tr ON p.ID = tr.object_id INNER JOIN $wpdb->term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id";

		if ( $in_same_cat ) {
			if ( ! is_object_in_taxonomy( $post->post_type, 'category' ) )
				return '';
			$cat_array = wp_get_object_terms($post->ID, 'category', array('fields' => 'ids'));
			if ( ! $cat_array || is_wp_error( $cat_array ) )
				return '';
			$join .= " AND tt.taxonomy = 'category' AND tt.term_id IN (" . implode(',', $cat_array) . ")";
		}

		$posts_in_ex_cats_sql = "AND tt.taxonomy = 'category'";
		if ( ! empty( $excluded_categories ) ) {
			if ( ! is_array( $excluded_categories ) ) {
				// back-compat, $excluded_categories used to be IDs separated by " and "
				if ( strpos( $excluded_categories, ' and ' ) !== false ) {
					_deprecated_argument( __FUNCTION__, '3.3', sprintf( __( 'Use commas instead of %s to separate excluded categories.' ), "'and'" ) );
					$excluded_categories = explode( ' and ', $excluded_categories );
				} else {
					$excluded_categories = explode( ',', $excluded_categories );
				}
			}
			

			$excluded_categories = array_map( 'intval', $excluded_categories );

			if ( ! empty( $cat_array ) ) {
				//$excluded_categories = array_diff($excluded_categories, $cat_array);
				$posts_in_ex_cats_sql = '';
			}
			//var_dump($excluded_categories, $cat_array);

			if ( !empty($excluded_categories) ) {
				$posts_in_ex_cats_sql = " AND tt.taxonomy = 'category' AND tt.term_id NOT IN (" . implode($excluded_categories, ',') . ')';
			}
		}
	}

	$adjacent = $previous ? 'previous' : 'next';
	$op = $previous ? '<' : '>';
	$order = $previous ? 'DESC' : 'ASC';

	$join  = apply_filters( "get_{$adjacent}_post_join", $join, $in_same_cat, $excluded_categories );
	$where = apply_filters( "get_{$adjacent}_post_where", $wpdb->prepare("WHERE p.post_date $op %s AND p.post_type = %s AND p.post_status = 'publish' $posts_in_ex_cats_sql", $current_post_date, $post->post_type), $in_same_cat, $excluded_categories );
	$sort  = apply_filters( "get_{$adjacent}_post_sort", "ORDER BY p.post_date $order LIMIT 1" );

	$query = "SELECT p.id FROM $wpdb->posts AS p $join $where $sort";
	$query_key = 'adjacent_post_' . md5($query);
	$result = wp_cache_get($query_key, 'counts');
	if ( false !== $result ) {
		if ( $result )
			$result = get_post( $result );
		return $result;
	}

	$result = $wpdb->get_var( $query );
	if ( null === $result )
		$result = '';

	wp_cache_set($query_key, $result, 'counts');

	if ( $result )
		$result = get_post( $result );

	return $result;
}

function next_post_link_same_cat($format='%link &raquo;', $link='%title', $in_same_cat = false, $excluded_categories = '') {
	adjacent_post_link_same_cat($format, $link, $in_same_cat, $excluded_categories, false);
}

function previous_post_link_same_cat($format='&laquo; %link', $link='%title', $in_same_cat = false, $excluded_categories = '') {
	adjacent_post_link_same_cat($format, $link, $in_same_cat, $excluded_categories, true);
}

function adjacent_post_link_same_cat( $format, $link, $in_same_cat = false, $excluded_categories = '', $previous = true ) {
	if ( $previous && is_attachment() )
		$post = get_post( get_post()->post_parent );
	else
		$post = get_adjacent_post_same_cat( $in_same_cat, $excluded_categories, $previous );

	if ( ! $post ) {
		$output = '';
	} else {
		$title = $post->post_title;

		if ( empty( $post->post_title ) )
			$title = $previous ? __( 'Previous Post' ) : __( 'Next Post' );

		$title = apply_filters( 'the_title', $title, $post->ID );
		$date = mysql2date( get_option( 'date_format' ), $post->post_date );
		$rel = $previous ? 'prev' : 'next';

		$string = '<a href="' . get_permalink( $post ) . '" rel="'.$rel.'">';
		$inlink = str_replace( '%title', $title, $link );
		$inlink = str_replace( '%date', $date, $inlink );
		$inlink = $string . $inlink . '</a>';

		$output = str_replace( '%link', $inlink, $format );
	}

	$adjacent = $previous ? 'previous' : 'next';

	echo apply_filters( "{$adjacent}_post_link", $output, $format, $link, $post );
}
/*--! endregion 
	functions to correct pre / next post link */
	
// script for Active editor selection of the unicode characters keyboard plugin


/* theme thumbnail support */

add_theme_support( 'post-thumbnails' );
add_image_size( 'single-post-thumbnail', 150, 200, true );


/* !--beginregion 
 * functions to get the tags to be displayed in same order as inserted
 * implemented in shortcode key-word
 */

function wp_get_object_terms_tags_order_as_inserted($object_ids, $taxonomies, $args = array()) {
	global $wpdb;

	if ( empty( $object_ids ) || empty( $taxonomies ) )
		return array();

	if ( !is_array($taxonomies) )
		$taxonomies = array($taxonomies);

	foreach ( (array) $taxonomies as $taxonomy ) {
		if ( ! taxonomy_exists($taxonomy) )
			return new WP_Error('invalid_taxonomy', __('Invalid taxonomy'));
	}

	if ( !is_array($object_ids) )
		$object_ids = array($object_ids);
	$object_ids = array_map('intval', $object_ids);

	$defaults = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
	
	$args = wp_parse_args( $args, $defaults );
	
	
	

	$terms = array();
	if ( count($taxonomies) > 1 ) {
		foreach ( $taxonomies as $index => $taxonomy ) {
			$t = get_taxonomy($taxonomy);
			if ( isset($t->args) && is_array($t->args) && $args != array_merge($args, $t->args) ) {
				unset($taxonomies[$index]);
				$terms = array_merge($terms, wp_get_object_terms($object_ids, $taxonomy, array_merge($args, $t->args)));
			}
		}
	} else {
		$t = get_taxonomy($taxonomies[0]);
		if ( isset($t->args) && is_array($t->args) )
			$args = array_merge($args, $t->args);
		
	}

	extract($args, EXTR_SKIP);

	if ( 'count' == $orderby )
		$orderby = 'tt.count';
	else if ( 'name' == $orderby )
		$orderby = 't.name';
	else if ( 'slug' == $orderby )
		$orderby = 't.slug';
	else if ( 'term_group' == $orderby )
		$orderby = 't.term_group';
	else if ( 'term_order' == $orderby )
		$orderby = 'tr.term_order';
	else if ( 'none' == $orderby ) {
		$orderby = '';
		$order = '';
	} else {
		$orderby = 't.term_id';
	}

	// tt_ids queries can only be none or tr.term_taxonomy_id
	if ( ('tt_ids' == $fields) && !empty($orderby) )
		$orderby = 'tr.term_taxonomy_id';

	if ( !empty($orderby) )
		$orderby = "ORDER BY $orderby";

	$order = strtoupper( $order );
	if ( '' !== $order && ! in_array( $order, array( 'ASC', 'DESC' ) ) )
		$order = 'ASC';

	$taxonomies = "'" . implode("', '", $taxonomies) . "'";
	$object_ids = implode(', ', $object_ids);

	$select_this = '';
	if ( 'all' == $fields ) {
		$select_this = 't.*, tt.*, tr.relation_id';
		$orderby = 'ORDER BY';
		$order = 'relation_id';
	}
	else if ( 'ids' == $fields )
		$select_this = 't.term_id';
	else if ( 'names' == $fields )
		$select_this = 't.name';
	else if ( 'slugs' == $fields )
		$select_this = 't.slug';
	else if ( 'all_with_object_id' == $fields )
		$select_this = 't.*, tt.*, tr.object_id';

	$query = "SELECT $select_this FROM $wpdb->terms AS t INNER JOIN $wpdb->term_taxonomy AS tt ON tt.term_id = t.term_id INNER JOIN $wpdb->term_relationships AS tr ON tr.term_taxonomy_id = tt.term_taxonomy_id WHERE tt.taxonomy IN ($taxonomies) AND tr.object_id IN ($object_ids) $orderby $order";
	if ( 'all' == $fields || 'all_with_object_id' == $fields ) {
		$terms = array_merge($terms, $wpdb->get_results($query));
		update_term_cache($terms);
	} else if ( 'ids' == $fields || 'names' == $fields || 'slugs' == $fields ) {
		$terms = array_merge($terms, $wpdb->get_col($query));
	} else if ( 'tt_ids' == $fields ) {
		$terms = $wpdb->get_col("SELECT tr.term_taxonomy_id FROM $wpdb->term_relationships AS tr INNER JOIN $wpdb->term_taxonomy AS tt ON tr.term_taxonomy_id = tt.term_taxonomy_id WHERE tr.object_id IN ($object_ids) AND tt.taxonomy IN ($taxonomies) $orderby $order");
	}

	if ( ! $terms )
		$terms = array();

	return apply_filters('wp_get_object_terms', $terms, $object_ids, $taxonomies, $args);
}

function wp_get_post_tags_order_as_inserted( $post_id = 0, $args = array() ) {
	return wp_get_object_terms_tags_order_as_inserted( $post_id, 'post_tag', $args);
}

/* !--endregion */


/* get all category issue , ordered latest issue first , return array objects of issue category */
function get_all_issues_latest_first($top_cat_ID=21) {
	$all_issues = array();
	$top_category = get_category($top_cat_ID);
	$subcategories = get_categories(array('parent' => $top_category->cat_ID, 'hide_empty' => false));
	usort($subcategories, "compareCategoryName"); // sorting function "compareCategoryName" is stored in functions.php of theme
	foreach ($subcategories as $subcategory) {
		$subsubcats = get_categories(array('parent' => $subcategory->cat_ID, 'hide_empty' => false));
		usort($subsubcats, "compareCategoryName"); // sorting function "compareCategoryName" is stored in functions.php of theme
		array_reverse($subsubcats);
		$all_issues = array_merge($all_issues,$subsubcats);
		//var_dump($all_issues);
	}
	return $all_issues;
}

/* get the lastest issue that have posts , use function get_all_issues_latest_first */
function get_lastest_issue() {
	$issues = get_all_issues_latest_first();
	foreach ($issues as $issue) {
		if ($issue->count != 0) {
			return $issue;
		}
	}
}

/* Sort posts in wp_list_table by column in ascending or descending order. */
function custom_post_order($query){
    /* 
        Set post types.
        _builtin => true returns WordPress default post types. 
        _builtin => false returns custom registered post types. 
    */
    $post_types = get_post_types(array('_builtin' => true), 'names');
    /* The current post type. */
    $post_type = $query->get('post_type');
    /* Check post types. */
    if(in_array($post_type, $post_types)){
        /* Post Column: e.g. date */
        if($query->get('orderby') == ''){
            $query->set('orderby', 'date');
        }
        /* Post Order: ASC / DESC */
        if($query->get('order') == ''){
            $query->set('order', 'ASC');
        }
    }
}
if(is_admin()){
    add_action('pre_get_posts', 'custom_post_order');
}

/* !-- begin region 
 * custom category fields
 */
 
//add extra fields to category edit form hook
add_action ( 'edit_category_form_fields', 'extra_category_fields');
//add extra fields to category edit form callback function
function extra_category_fields( $tag ) {    //check for existing featured ID
    $t_id = $tag->term_id;
    $cat_meta = get_option( "category_$t_id");
?>
<tr class="form-field">
<th scope="row" valign="top"><label for="journal-content"><?php _e('Journal content shortcode'); ?></label></th>
<td>
<input type="text" name="Cat_meta[journal-content]" id="Cat_meta[journal-content]" size="25" style="width:60%;" value="<?php echo $cat_meta['journal-content'] ? $cat_meta['journal-content'] : ''; ?>"><br />
            <span class="description"><?php _e('[wpfilebase tag=file id=_insert_id_here_ tpl=journal-content /]'); ?></span>
        </td>
</tr>
<tr class="form-field">
<th scope="row" valign="top"><label for="news-and-information"><?php _e('News and information shortcode'); ?></label></th>
<td>
<input type="text" name="Cat_meta[news-and-information]" id="Cat_meta[news-and-information]" size="25" style="width:60%;" value="<?php echo $cat_meta['news-and-information'] ? $cat_meta['news-and-information'] : ''; ?>"><br />
            <span class="description"><?php _e('[wpfilebase tag=file id=_insert_id_here_ tpl=news-and-info /]'); ?></span>
        </td>
</tr>

<?php
}

// save extra category extra fields hook
add_action ( 'edited_category', 'save_extra_category_fileds');
   // save extra category extra fields callback function
function save_extra_category_fileds( $term_id ) {
    if ( isset( $_POST['Cat_meta'] ) ) {
        $t_id = $term_id;
        $cat_meta = get_option( "category_$t_id");
        $cat_keys = array_keys($_POST['Cat_meta']);
            foreach ($cat_keys as $key){
            if (isset($_POST['Cat_meta'][$key])){
                $cat_meta[$key] = $_POST['Cat_meta'][$key];
            }
        }
        //save the option array
        update_option( "category_$t_id", $cat_meta );
    }
}

/* !-- end region */


/* custome post in category */
function my_post_queries( $query ) {
  // do not alter the query on wp-admin pages and only alter it if it's the main query
  if (!is_admin() && $query->is_main_query()){

    // alter the query for the home and category pages 

    if(is_home()){
      $query->set('posts_per_page', 3);
    }

    if(is_category('books')){
      $query->set('posts_per_page', 6);
    }

  }
}
add_action( 'pre_get_posts', 'my_post_queries' );

/*End custome post in category */

// function for cutting the text in breadcrumbs
add_action('bcn_after_fill', 'foo_pop');
function foo_pop($trail)
{
    array_shift($trail->trail);
}

// remove wpautop
//remove_filter( 'the_excerpt', 'wpautop' );
//remove_filter( 'the_content', 'wpautop' );

// function for creating / updating the excerpt automatically

function auto_insert_excerpt2(){
	$post_data = &$_POST;
	
	//var_dump($post_data['cfs']);
	if ($post_data['cfs'] && post_is_in_descendant_category(21)) {
		$output ='';
		if ($post_data['cfs']['input']) {
			$file_id = $post_data['cfs']['input'][1]['value'];
			// $author = $post_data['cfs']['input'][2]['value'];
		}
	
	
		//$output = '[do action="generate-excerpt" postid="'.$post_data['post_ID'].'" pdfid="'.$file_id .'" author="'.$author.'"/]';
		$output = '[do action="generate-excerpt" postid="'.$post_data['post_ID'].'" pdfid="'.$file_id .'"/]';
		
		return $output;
	} else {
		return $post_data['post_excerpt'];
	}
	
}
add_filter('excerpt_save_pre', 'auto_insert_excerpt2');


// duplicate post hook function
function duplicate_post_copy_post_meta_info2($new_id, $post) {
	global $cfs;
	$value = $cfs->get('page_number', $post->ID);
	$fields = array('page_number' => $value);
	//var_dump($fields);
	if ($fields['page_number']) {
		$cfs->save($fields, array('ID' => $new_id));
	}
}
/**/
// Using our action hooks to copy meta fields
add_action('dp_duplicate_post', 'duplicate_post_copy_post_meta_info2', 10, 2);
add_action('dp_duplicate_page', 'duplicate_post_copy_post_meta_info2', 10, 2);

// function to get draft page content to be displayed and add edit link to that draft page if is admin
function get_reusable_part($page_name, $previous ='', $after ='') {
	$page = get_page_by_title( $page_name,  'OBJECT', 'page' );
	$result = '';
	$result .= do_shortcode($page->post_content);
	$post_type_object = get_post_type_object( $page->post_type );
	if ( current_user_can( $post_type_object->cap->edit_post, $page->ID ) ) {
		$result .= $previous . '<span class="edit-link"><a href="' . get_edit_post_link($page->ID) . '">Edit '. $page_name .'</a></span>' . $after;
	}
	return $result;
}


// function to hide admin bar from subscribers:
// show admin bar only for admins and editors
if (!current_user_can('edit_posts')) {
	add_filter('show_admin_bar', '__return_false');
}


// Remove/Disable all comments from all posts/pages
add_filter('comments_open', '__return_false');

// get the parent category name function
function get_parent_category_name($cat,$pointer=0) {
    $parentCategoryList = get_category_parents($cat, false, ',');
    $parentCategoryListArray = split(',', $parentCategoryList);
    $parentName = $parentCategoryListArray[$pointer];
    $stuffToReplace = array(' ' => '-', '(' => '', ')' => '');
    $parent = strtolower(strtr($parentName,$stuffToReplace));

    return $parent;
}



?>