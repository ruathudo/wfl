<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */
$category = get_category( get_query_var( 'cat' ) );
$cat_id = $category->cat_ID;
// start journal-view

// start year-view
// List of issues in each year will be display here ( ex: Year 2003 - Issue 1 - Issue 2 - ... )
 $args = array(
'type'                     => 'post',
'child_of'                 => $cat_id,
'parent'                   => $cat_id,
'orderby'                  => 'name',
'order'                    => 'ASC',
'hide_empty'               => 0,
'hierarchical'             => 1,
'exclude'                  => '',
'include'                  => '',
'number'                   => '',
'taxonomy'                 => 'category',
'pad_counts'               => false );
$categories = get_categories( $args ); 
?>

<div class="year-header">
	Journal Issues of year <?php echo $category->cat_name;?>:
</div>

<div class="clearfix year-issues-wrapper">
	<?php
	
	//if ( $categories[0] && strlen($categories[0]->cat_name) ==4)
	//	usort($categories, "compareCategoryName");

	foreach ($categories as $category) {
		echo '<div class="issue-cover">';
			echo '<a href="' . get_category_link( $category->cat_ID ). '">';
			echo '<image class="issue-image" alt="" src="'. z_taxonomy_image_url($category->term_id) .'"></image>';
			echo '<div class="issue-cover-footer">';
				echo '<p>' . $category->cat_name . '</p></a>';
				
				// get ISSN print and online form these 2 pages
				$printISSNpage = get_page_by_title( 'Print ISSN',  'OBJECT', 'page' ); 
				$onlineISSNpage = get_page_by_title( 'Online ISSN',  'OBJECT', 'page' );
				$output = '<p>Print ISSN:' . $printISSNpage->post_content . '</p>';
				$output .= '<p>Online ISSN:' . $onlineISSNpage->post_content . '</p>';
				
				echo $output;
			echo '</div>';
		echo '</div>';
	}
	?> 
</div>
<?php
//if ( category_description() || true ) : // Show an optional category description ?>
	<!-- <div class="year-category-view-footer"><?php //echo category_description(); ?></div> -->
<?php //endif; ?>

<div id="year-nav-div-wrapper" class="clearfix">
<?php
	$parent_cat_ID = $parent_categories[0];
	$child_cats = get_categories(array('parent' => $parent_cat_ID, 'hide_empty' => false));
	//var_dump($child_cats);
	usort($child_cats, "compareCategoryName");
	$i=0;
	foreach ($child_cats as $child_cat) {
		if ($child_cat->cat_ID == $cat_id) {
			if ($i < (count($child_cats)-1) ) {
			?>
				<div id="prev-year-link">
					<a href="<?php echo get_category_link($child_cats[$i+1]->cat_ID) ?>">Previous</a>
				</div>
			<?php
			}
			if ($i >0) {
			?>
				<div id="next-year-link">
					<a href="<?php echo get_category_link($child_cats[$i-1]->cat_ID) ?>">Next</a>
				</div>
			<?php
			}
			break;
		} // !-- endif;
		$i++;
	} 
?>
</div>