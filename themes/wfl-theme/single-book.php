<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */

get_header(); ?>
    <div id="thirdary" class="widget-area-left" role="complementary">
        <?php dynamic_sidebar( 'sidebar-2' ); ?>
    </div><!-- #secondary -->
	<div id="primary" class="site-content">

		<div id="content" role="main">
			<nav class="nav-single">
				<span class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'wfl_theme' ) . '</span> Back' , TRUE); ?></span>
					<span class="nav-next"><?php next_post_link( '%link', 'Next <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'wfl_theme' ) . '</span>' , TRUE); ?></span>
			</nav><!-- .nav-single -->

			

			<?php while ( have_posts() ) : the_post(); ?>
				
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
                </header><!-- .entry-header -->
                <div class="entry-content">
                <script>
                function OnTab1Click()
				{
					document.getElementById("DescriptionTab").className = "tabby1";
					document.getElementById("ToCTab").className = "tabby2";
					document.getElementById("descontent").className = "";
					document.getElementById("toccontent").className = "div_no_display";
				}
				function OnTab2Click()
				{
					document.getElementById("DescriptionTab").className = "tabby2";
					document.getElementById("ToCTab").className = "tabby1";
					document.getElementById("descontent").className = "div_no_display";
					document.getElementById("toccontent").className = "";
				}
                </script>
                
                <div id="tabcontain" class="bookleft">
                	<ul class="tabs">
                		<li id="DescriptionTab" class="tabby1"><a onclick="OnTab1Click()" id="content-desc" class="tab-anchor-activator" title="Description">Description</a></li>
                		<li id="ToCTab" class="tabby2"><a onclick="OnTab2Click()" id="content-cont" class="tab-anchor-activator" title="Table of Contents">Table of Contents</a></li>
                	</ul>
                </div>
                
                <div id="tabcontent">
                    <div id="descontent">
                        <?php the_content(); ?>
                    </div>
                    <div id="toccontent" class="div_no_display">
                        <?php if(!empty($post->post_excerpt) ) {
								the_excerpt();
							}
                        ?>
                    </div>
                </div>
					<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'wfl_theme' ), 'after' => '</div>' ) ); ?>
                    
				</div><!-- .entry-content -->

				

				<?php comments_template( '', true ); ?>

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php 

	echo '<div id="book-detail-wrap" class="right-side">';
	echo '<div class="book-image-widget">';
	the_post_thumbnail(array(150,200));
	echo '</div>';
	$meta = get_post_custom_values('book_price');
	if(!empty($meta[0]))
		echo '<p style="color: #444;">' . '<strong>Cost:</strong><p style="color:#660000">' . nl2br($meta[0]) . '</p></p>';
	$meta = get_post_custom_values('book_detail');
	if(!empty($meta[0]))
		echo '<p style="color: #444;">' . '<strong>Details:</strong>' . nl2br($meta[0]) . '</p>';
	$meta = get_post_custom_values('book_incd');
	if(!empty($meta[0]))
		echo '<p style="color: #444;">' . '<strong>InCD:</strong>' . nl2br($meta[0]) . '</p>';
	echo '</div>';
	

?>
<?php get_footer(); ?>