<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */
$category = get_category( get_query_var( 'cat' ) );
$cat_id = $category->cat_ID;
// List of issues in each year will be display here ( ex: Year 2003 - Issue 1 - Issue 2 - ... )
if ( have_posts() || true ) : ?>
<header class="archive-header issue-header-wrapper">
	<!--<h1 class="archive-title"><?php //printf( __( 'Category Archives: %s', 'wfl_theme' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>-->
	<?php
	echo get_reusable_part('Issue header', '','');
	?>
	<div id="issue-below-header-wrapper" class="clearfix">
		<div id="issue-below-left-side-display" >
			<?php
			if ( category_description() ) : // Show an optional category description ?>
				<div class="Issue-info"><?php echo category_description(); ?></div>
			<?php 
			endif; ?>
			<div><?php 
					
				echo get_reusable_part('Issue header2', '','<br/>');
			?>
			</div>
			
			<div id="top-nav-checkboxs">

			</div><!-- end top-nav-checkboxs -->
		</div><!-- end issue-below-left-side-display -->
		
		<div id="issue-below-right-side-display">
			<image class="issue-image" alt="" src="<?php echo z_taxonomy_image_url($category->term_id); ?>"></image>
			<div class="impact-factor"><?php echo apply_filters( 'the_content','[do action="impact-factor"/]'); ?></div>
		</div><!-- end issue-below-right-side-display -->
	</div> <!-- end issue-below-header-wrapper -->
</header><!-- .archive-header -->


<?php // ------ Section Agriculture display-------- ?>
<div>
	<hr class="issue-hr"/>
	<div class="Issue-section-header">
		<a name="Agriculture" ></a>
		<p class="Issue-section-anchors">Articles</p>
	</div>
	<hr class="issue-hr"/>
	<?php
	$query = new WP_Query( array( 'posts_per_page'=>-1, 'cat'=>$cat_id , 'orderby' => 'date', 'order'=>'ASC' ) );
	/* Start the Loop */
	echo '<div>';
	//usort($query->posts, "comparePostPage"); // sorting the posts according to 'page' custom field
	while ( $query->have_posts() ) 
	{
		$query->the_post();
		get_template_part( 'content', 'pdf' );
	}
	echo '</div>';
	wp_reset_query();
	wp_reset_postdata();
	?>
</div>

<!--<hr class="issue-hr"/>-->
<?php 
	
	// Print out the footer section of category Issues
	echo get_reusable_part('Issue footer', '','');
?>
<?php 
else : ?>
	<?php get_template_part( 'content', 'none' ); ?>
<?php 
endif; ?>