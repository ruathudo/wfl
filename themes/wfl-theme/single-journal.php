<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */

get_header(); ?>
    <div id="thirdary" class="widget-area-left" role="complementary">
        <?php dynamic_sidebar( 'sidebar-2' ); ?>
    </div><!-- #secondary -->
	<div id="primary" class="site-content">

		<div id="content" role="main">
			<nav class="nav-single">
				<span class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'wfl_theme' ) . '</span> Back' , TRUE); ?></span>
					<span class="nav-next"><?php next_post_link( '%link', 'Next <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'wfl_theme' ) . '</span>' , TRUE); ?></span>
			</nav><!-- .nav-single -->

			

			<?php while ( have_posts() ) : the_post(); ?>
				
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
                </header><!-- .entry-header -->
                <article class="entry-content">
					<?php if($cfs->get()) { ?>
					
						<div class="author-article-line">
							<span class="author-article-word">Author: </span><?php echo $cfs->get('author' , get_the_ID(), array('format' => 'raw')); ?>
						</div>
						<div class="date-received-article-line">
							<?php echo $cfs->get('date_received', get_the_ID(), array('format' => 'raw')); ?>
						</div>
						
						
						<div class="abstract-header-article-line">Abstract</div>
					<?php } // endif; ?>
					
					<div class="abstract-content">
						<?php the_content() ?>
					</div>
					
					<?php if($cfs->get()) { ?>
						<div class="article-keywords">
							<?php echo do_shortcode( '[do action="key-word"/][wpfilebase tag=file id=' . $cfs->get('file_id', get_the_ID(), array('format' => 'raw')) . ' /]' ); ?>
						</div>
						<div class="">
							<?php 
								echo do_shortcode( '[do action="article-middle1"/]' );
								echo $cfs->get('page_number', get_the_ID(), array('format' => 'raw')); 
								echo do_shortcode( '[do action="article-middle2"/]' );
							?>
						</div>
						<?php echo do_shortcode( '[do action="article-footer"/]' ); ?>
						<?php edit_post_link( __( 'Edit', 'wfl_theme' ), '<span class="edit-link">', '</span>' ); ?>
					<?php } // endif; ?>
					
				</article><!-- .entry-content -->

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->
<?php get_sidebar();?>
<?php get_footer(); ?>