<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */

get_header(); ?>

	<aside id="thirdary" class="left-side clearfix" role="complementary">
		<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</aside><!-- #left side-bar -->	

	<div id="primary" class="middle clearfix">
		

		
			<div id="gallery" class="clearfix">	
				<?php if (function_exists('slideshow')) { 
					slideshow($output = true, $gallery_id = 1, $post_id = false, $params = array()); 
				} ?>
			</div><!-- plugin slides gallery -->

			
			
			<div class="header-title">
				<img alt="Article" src="<?php echo get_bloginfo('template_url'); ?>/images/paperclip.png"></img>
				<p>Featured Articles</p>
				
			</div><!--Featured Articles-->
			
			<div id="articles" class="clearfix"> <!--Articles content-->
				
				<div class="article">

					
					<?php
						$section_id = get_cat_ID('Featured Articles');
						$cat_id = get_cat_ID('Food and Health');
						$query = new WP_Query( array( 'posts_per_page'=> 1, 'category__and' => array( $cat_id, $section_id ) ) );
						while ( $query->have_posts() ) 
						{
							$query->the_post();
							get_template_part( 'content', 'featured' );
						}
						wp_reset_query();
						wp_reset_postdata();
					?>
					
				</div>


				<div class="article">
					
					<?php
						$section_id = get_cat_ID('Featured Articles');
						$cat_id = get_cat_ID('Agriculture');
						$query = new WP_Query( array( 'posts_per_page'=> 1, 'category__and' => array( $cat_id, $section_id ) ) );
						while ( $query->have_posts() ) 
						{
							$query->the_post();
							get_template_part( 'content', 'featured' );
						}
						wp_reset_query();
						wp_reset_postdata();
					?>
					
				</div>


				<div class="article">
				
					<?php
						$section_id = get_cat_ID('Featured Articles');
						$cat_id = get_cat_ID('Environment'); // EnvironmentalEnvironment
						$query = new WP_Query( array( 'posts_per_page'=> 1, 'category__and' => array( $cat_id, $section_id ) ) );
						while ( $query->have_posts() ) 
						{
							$query->the_post();
							get_template_part( 'content', 'featured' );
						}
						wp_reset_query();
						wp_reset_postdata();
					?>
					
				</div>
				
			</div><!-- #article -->
		
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>