<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
		<div class="featured-post">
			<?php _e( 'Featured post', 'wfl_theme' ); ?>
		</div>
		<?php endif; ?>
		

		<?php if ( is_search() ) : // Only display Excerpts for Search ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
		<?php else : ?>
		<div class="article-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php if (has_post_thumbnail( get_the_ID() ) ): ?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full');//'single-post-thumbnail' ); ?>
				<image class="feature-article-image" id="custom-bg" src="<?php echo $image[0]; ?>" alt="thumbnail-img">
				<?php endif; ?>
			</a>
		</div>
		<div class="featured-article-title">
			<p>
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'wfl_theme' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php echo get_the_title(); ?></a>
			</p>
			
		</div><!-- .entry-header -->
		<?php endif; ?>
	</article><!-- #post -->