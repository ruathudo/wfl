<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */
$category = get_category( get_query_var( 'cat' ) );
$cat_id = $category->cat_ID;
// start journal-view

// start year-view
// List of issues in each year will be display here ( ex: Year 2003 - Issue 1 - Issue 2 - ... )
 $args = array(
'type'                     => 'post',
'child_of'                 => $cat_id,
'parent'                   => $cat_id,
'orderby'                  => 'name',
'order'                    => 'ASC',
'hide_empty'               => 0,
'hierarchical'             => 1,
'exclude'                  => '',
'include'                  => '',
'number'                   => '',
'taxonomy'                 => 'category',
'pad_counts'               => false );
$categories = get_categories( $args ); 
?>

<div class="year-header">
	Journals of year <?php echo $category->cat_name;?>:
</div>

<div class="clearfix year-issues-wrapper">
	<?php
	
	//if ( $categories[0] && strlen($categories[0]->cat_name) ==4)
	//	usort($categories, "compareCategoryName");
//var_dump($categories[0]);
	foreach ($categories as $category) {

		echo '<div class="issue-cover">';
			echo '<a href="' . get_category_link( $category->cat_ID ). '">';
			echo '<image class="issue-image" alt="" src="'. z_taxonomy_image_url($category->term_id) .'"></image>';
			echo '<div class="issue-cover-footer">';
				echo '<p>' . $category->cat_name . '</p></a>';
				
				// get ISSN print and online form these 2 pages
				$printISSNpage = get_page_by_title( 'Print ISSN',  'OBJECT', 'page' ); 
				$onlineISSNpage = get_page_by_title( 'Online ISSN',  'OBJECT', 'page' );
				$output = '<p>Print ISSN:' . $printISSNpage->post_content . '</p>';
				$output .= '<p>Online ISSN:' . $onlineISSNpage->post_content . '</p>';
				
				echo $output;
			echo '</div>';
		echo '</div>';
	}
	?> 

</div>