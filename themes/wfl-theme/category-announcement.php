<?php
/**
 * The template for displaying Category pages.
 *
 * Used to display archive-type pages for posts in a category.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */

get_header(); ?>
	<div id="thirdary" class="left-side clearfix" role="complementary">
			<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</div><!-- #secondary -->
	<section id="primary" class="site-content">
		<div id="content" role="main" class="clearfix">
		<?php if ( have_posts() ) : ?>
			<header class="archive-header">

			<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php echo category_description(); ?></div>
			<?php endif; ?>
            
			</header><!-- .archive-header -->
			<?php
			while ( have_posts() ) 
			{
				the_post();
				get_template_part( 'content', 'announcement' );
			}
			wfl_theme_content_nav( 'nav-below' );
			?>

			<?php else : ?>
                <?php get_template_part( 'content', 'none' ); ?>
            <?php endif; ?>
		</div><!-- #content -->  
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>