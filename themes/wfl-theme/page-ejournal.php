<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */

get_header(); ?>
    <div id="thirdary" class="widget-area-left" role="complementary">
			<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</div><!-- #secondary -->
	<div id="primary" class="site-content">
		<div id="content" role="main">

		<?php
		$category = get_category( get_query_var( 'cat' ) );
		$cat_id = $category->cat_ID;
		$parent_categories = get_ancestors( $cat_id, 'category' );
		$number_of_parents = count( $parent_categories);
		var_dump($category);


		?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>