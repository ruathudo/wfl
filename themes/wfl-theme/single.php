<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */

$post = $wp_query->post;
if (in_category('rents')) {
	include(TEMPLATEPATH.'/single-rent.php');
} 
else if(in_category('books')){
	include(TEMPLATEPATH.'/single-book.php');
}
else if(in_category('journals') || post_is_in_descendant_category(21) ){
	include(TEMPLATEPATH.'/single-journal.php');
}
else{

	get_header(); ?>

	<div id="thirdary" class="widget-area-left" role="complementary">
			<?php dynamic_sidebar( 'sidebar-2' ); ?>
		</div><!-- #secondary -->
		<div id="primary" class="middle clearfix">
			<div id="content" role="main">

				<nav class="nav-single">
					<span class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'wfl_theme' ) . '</span> Back' , TRUE); ?></span>
					<span class="nav-next"><?php next_post_link( '%link', 'Next <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'wfl_theme' ) . '</span>' , TRUE); ?></span>
				</nav><!-- .nav-single -->
				

				<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>

					<?php comments_template( '', true ); ?>

				<?php endwhile; // end of the loop. ?>

			</div><!-- #content -->
		</div><!-- #primary -->
		<?php get_sidebar();?>
		<?php get_footer();
} //End else
?>
