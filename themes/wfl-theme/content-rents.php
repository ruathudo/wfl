<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */
?>
	

		
<?php global $id_counter; ?>
<tr class="house-item" itemscope="" itemtype="" valign="top">
	<td class="image">
		<script type="text/javascript">
			rentalImages = new Array();
			rentalImageSeq[<?php echo $id_counter;?>] = 1;
			<?php
				$meta = get_post_custom_values('house_images');
				if(!empty($meta[0]))
				{
					$images = explode(";", $meta[0]);
					$counter = 1; 
					foreach($images as $image_url)
					{
						echo 'var image'. $counter .' = \''. $image_url .'\';';
						echo 'rentalImages['. $counter .'] = image'.$counter.';';
						$counter++; //var_dump($images[0]);
					}
					$currentImgUrl = $images[0]; //var_dump($currentImgUrl);
				}
				else
				{
					echo 'var image1 = \'default_image_url.jpg\';';
					echo 'rentalImages[1] = image1;';
					$currentImgUrl = 'default_image_url.jpg';
				}
				
			?>
			allRentalImages[<?php echo $id_counter;?>] = rentalImages;
		</script>

		<img id="<?php echo 'image' . $id_counter;?>" src="<?php echo $currentImgUrl; ?>" />
		<a href="" onClick="javascript:changePreviousImage(<?php echo $id_counter;?>,rentalImageSeq[<?php echo $id_counter;?>]); return false;">&larr;</a>
		<a href="" onClick="javascript:changeNextImage(<?php echo $id_counter;?>,rentalImageSeq[<?php echo $id_counter;?>]); return false;">&rarr;</a>

	</td>
	<td>
		<a href="<?php the_permalink() ?>">
		<?php
			$meta = get_post_custom_values('house_type');
			echo '<p>' . '' . nl2br($meta[0]) . '</p>';
		?>
		<?php
			$meta = get_post_custom_values('house_rooms');
			if(!empty($meta[0]))
				echo '<p>' . nl2br($meta[0]) . '</p>';
		?>
		</a>
	</td>
	<td>
		<?php
			$meta = get_post_custom_values('house_area');
			if(!empty($meta[0]))
				echo '<p>' . nl2br($meta[0]) . ' m<sup>2</sup></p>';
		?>
	</td>
	<td>
		<?php
			$meta = get_post_custom_values('house_rent_cost_short');
			if(!empty($meta[0])) {
				echo '<p>' . nl2br($meta[0]) . '</p>';
			}
		?>
	</td>
	<td>
		<?php
			$meta = get_post_custom_values('house_availability');
			if(!empty($meta[0]))
				echo '<p>' . nl2br($meta[0]) . '</p>';
		?>
	</td>
	<td>
		<?php
			$meta = get_post_custom_values('house_location');
			if(!empty($meta[0]))
				echo '<p>' . nl2br($meta[0]) . '</p>';
		?>
	</td>
</tr>
	
		
<style>
	img.preload { display: none; } /* preload images */
</style>

<?php //Preload images 
	if(!empty($images))
	{
		foreach($images as $image_url)
		{
			echo "<img src='". $image_url."' alt='' class='preload' /> ";
		}
	}
?>        

		

