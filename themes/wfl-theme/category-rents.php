<?php
/**
 * The template for displaying Category pages.
 *
 * Used to display archive-type pages for posts in a category.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */

get_header(); ?>
<style>	

	.wide_width_column {
		min-width: 65px;
	}

</style>

	<div id="thirdary" class="widget-area-left" role="complementary">
			<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</div><!-- #secondary -->
	<section id="primary" class="site-content">
		<div id="content" role="main" class="clearfix">
			<?php if ( have_posts() || true ) : ?>
				<h1 class="entry-title"><?php $cats = get_the_category(); echo $cats[0]->name; ?></h1>
				<header class="archive-header-1">

				<?php if ( category_description() ) : // Show an optional category description ?>
					<div class="archive-meta"><?php echo category_description(); ?></div>
				<?php endif; ?>
				
				</header><!-- .archive-header -->
				<br/>
				<script>
					var allRentalImages=new Array();
					var rentalImages=new Array();
					var rentalImageSeq=new Array();
					function changeNextImage(c,a){
						var b=getNextImageSeq(c,a);
						setImageUrl(c,getImageUrl(c,b));
						rentalImageSeq[c]=b
					}
					function changePreviousImage(c,a){
						var b=getPreviousImageSeq(c,a);
						setImageUrl(c,getImageUrl(c,b));
						rentalImageSeq[c]=b
					}
					function getNextImageSeq(d,a){
						var c=allRentalImages[d];
						var b=c.length-1;
						if(b<1){
							return null
						}
						if(a>=b){
							return 1
						}
						return a+1
					}
					function getPreviousImageSeq(d,a){
						var c=allRentalImages[d];
						var b=c.length-1;
						if(b<1){
							return null
						}
						if(a<=1){
							return b
						}
						return a-1
					}
					function getImage(c,a){
						var b=allRentalImages[c];
						return b[a]
					}
					function getImageUrl(b,a){return getImage(b,a)
					}
					function setImageUrl(b,a){document.getElementById("image"+b).src=a;
					return
					}
				</script>
				
				<table class="list-renting">
					<!-- <colgroup> --> 
						<!-- <col width="140"/> --> <!-- space -->
						<!-- <col width="200"/> --> <!-- name + type -->
						<!-- <col width="370"/> --> <!-- House area -->
						<!-- <col width="120"/> --> <!-- price -->
						<!-- <col width="65"/> --> <!-- availability -->
						<!-- <col width="200"/> --> <!-- location -->
						<!-- <col width="148"/> -->  <!-- Flat detail -->
					<!-- </colgroup> -->
					
					<thead>
						<tr class="house-header">
							<th></th>
							<th>Apartment</th>
							<th class="wide_width_column" >Area</th>
							<th>Rent/month</th>
							<th>Availability</th>
							<th>Location</th>
							<!--<th>Flat detail</th>-->
						</tr>
					</thead>
					
					<tbody>
						<?php
						$id_counter = 1;
						while ( have_posts() ) {
							the_post();
							get_template_part( 'content', 'rents' );
							$id_counter++;
						}
						?>
					</tbody>
				</table>
			<?php wfl_theme_content_nav( 'nav-below' ); ?>
			<?php else : ?>
				<?php get_template_part( 'content', 'none' ); ?>
			<?php endif; ?>
		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>