<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */
// start journal-view
// start year-view
// List of issues in each year will be display here ( ex: Year 2003 - Issue 1 - Issue 2 - ... )
$category = get_category( get_query_var( 'cat' ) );
$cat_id = $category->cat_ID;

if ( have_posts() || true ) : ?>
<header class="archive-header issue-header-wrapper">
	<!--<h1 class="archive-title"><?php //printf( __( 'Category Archives: %s', 'wfl_theme' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>-->
	<?php
	echo get_reusable_part('Issue header', '','');
	?>
	<div id="issue-below-header-wrapper" class="clearfix">
		<div id="issue-below-left-side-display" >
			<?php
			if ( category_description() ) : // Show an optional category description ?>
				<div class="Issue-info"><?php echo category_description(); ?></div>
			<?php 
			endif; ?>
			<div><?php 
					
				echo get_reusable_part('Issue header2', '','<br/>');
			?>
			</div>
			<?php
			
			// Check if there is commentary , if yes then flag $has_commentary for display view
			$section_id = get_cat_ID('Commentary');
			$query = new WP_Query( array( 'posts_per_page'=>-1, 'category__and' => array( $cat_id, $section_id ) ) );
			$has_commentary = $query->have_posts() ? true : false;
			wp_reset_query();
			wp_reset_postdata();
			
			// Check if there is research & news, if yes then flag $has_research for display view
			$section_id = get_category_by_slug( 'research-news')->cat_ID;
			$query = new WP_Query( array( 'posts_per_page'=>-1, 'category__and' => array( $cat_id, $section_id ) ) );
			$has_research = $query->have_posts() ? true : false;
			wp_reset_query();
			wp_reset_postdata();
			
			?>
			
			<div id="top-nav-checkboxs">
				<?php
					// display Journal content link
					$t_id = $category->term_id;
					$cat_meta = get_option( "category_$t_id");
					//var_dump($cat_meta['journal-content']);
					$journal_content = $cat_meta['journal-content'];
					if (!empty($journal_content))
						echo '<a href="'.$journal_content.'">Journal Contents</a>';
					else
						echo 'Journal Contents';
				?>
				<form>
					<p <?php if (!$has_commentary){ echo 'style="display:none"';} ?> ><input type="checkbox" onclick="toggle(this)" name="checkbox" id="comm-checkbox" />Commentary</p>
					<p><input type="checkbox" onclick="toggle(this)" name="checkbox" id="food-checkbox" />Food and Health</p>
					<p><input type="checkbox" onclick="toggle(this)" name="checkbox"  id="agri-checkbox" />Agriculture</p>
					<p><input type="checkbox" onclick="toggle(this)" name="checkbox" id="envi-checkbox" />Environment</p>
					<p <?php if (!$has_research){ echo 'style="display:none"';} ?> ><input type="checkbox" onclick="toggle(this)" name="checkbox" id="rese-checkbox" />Research & News</p>
					<p><input type="checkbox" onclick="toggle(this)" name="checkbox" checked="checked" id="all-checkbox" />All</p>
				</form>
				<?php
					$news_info = $cat_meta['news-and-information'];
					if (!empty($news_info))
						echo '<a href="'.$news_info.'">News and information</a>';
					else
						echo 'News and information';
					
				?>
				<style>.div_no_display{ display: none;}</style>
				<script>
					var previousCheckId = "all-checkbox";

					function toggle(chkBox) {
						if (chkBox.checked) {
							  if (previousCheckId != chkBox.getAttribute('id') && document.getElementById(previousCheckId).checked) {
								   document.getElementById(previousCheckId).checked = false;
							  }
							  previousCheckId = chkBox.getAttribute('id');
						}
						showSection();
					}
				
					function showSection()
					  {
						var showComm = document.getElementById("comm-checkbox").checked;
						var showFood = document.getElementById("food-checkbox").checked;
						var showAgri = document.getElementById("agri-checkbox").checked;
						var showEnvi = document.getElementById("envi-checkbox").checked;
						var showRese = document.getElementById("rese-checkbox").checked;
						if (showComm || showFood ||showAgri || showEnvi || showRese) {
							document.getElementById("all-checkbox").checked = false;
						}
						var showAll = document.getElementById("all-checkbox").checked;
						
						if(showAll == true)
						{
							document.getElementById("commentary").className = "";
							document.getElementById("food-and-health").className = "";
							document.getElementById("agriculture").className = "";
							document.getElementById("environment").className = "";
							document.getElementById("research-and-news").className = "";
						}
						else
						{
							document.getElementById("commentary").className = "";
							document.getElementById("food-and-health").className = "";
							document.getElementById("agriculture").className = "";
							document.getElementById("environment").className = "";
							document.getElementById("research-and-news").className = "";
							if(showComm == false)
								document.getElementById("commentary").className += " div_no_display";
							if(showFood == false)
								document.getElementById("food-and-health").className += " div_no_display";
							if(showAgri == false)
								document.getElementById("agriculture").className += " div_no_display";
							if(showEnvi == false)
								document.getElementById("environment").className += " div_no_display";
							if(showRese == false)
								document.getElementById("research-and-news").className += " div_no_display";
						}
					  }
					function showAllSections() {
						document.getElementById("comm-checkbox").checked = false;
						document.getElementById("food-checkbox").checked = false;
						document.getElementById("agri-checkbox").checked = false;
						document.getElementById("envi-checkbox").checked = false;
						document.getElementById("rese-checkbox").checked = false;
						showSection();
					}
				</script>
			</div><!-- end top-nav-checkboxs -->
		</div><!-- end issue-below-left-side-display -->
		
		<div id="issue-below-right-side-display">
			<image class="issue-image" alt="" src="<?php echo z_taxonomy_image_url($category->term_id); ?>"></image>
			<div class="impact-factor"><?php echo apply_filters( 'the_content','[do action="impact-factor"/]'); ?></div>
		</div><!-- end issue-below-right-side-display -->
	</div> <!-- end issue-below-header-wrapper -->
</header><!-- .archive-header -->

<?php // ------ Section commentary display-------- ?>
<div id="commentary" <?php if (!$has_commentary){ echo 'style="display:none"';} ?>>
	<hr class="issue-hr"/>
	<div class="Issue-section-header" >
		<a name="Commentary" ></a>
		<p class="Issue-section-anchors">Commentary</p>
	</div>
	<hr class="issue-hr"/>
	
	<?php
	$section_id = get_cat_ID('Commentary');
	$query = new WP_Query( array( 'posts_per_page'=>-1, 'category__and' => array( $cat_id, $section_id ), 'orderby' => 'date', 'order'=>'ASC' ) );
	/* Start the Loop */
	echo '<div>';
	//usort($query->posts, "comparePostPage"); // sorting the posts according to 'page' custom field
	while ( $query->have_posts() ) 
	{
		$query->the_post();
		get_template_part( 'content', 'pdf' );
	}
	echo '</div>';
	wp_reset_query();
	wp_reset_postdata();
	?>
</div>

<?php // ------ Section Food & Health display-------- ?>
<div id="food-and-health">
	<hr class="issue-hr"/>
	<div class="Issue-section-header">
		<a name="FoodandHealth" ></a>
		<p class="Issue-section-anchors">Food and Health</p>
	</div>
	<hr class="issue-hr"/>
	<?php
	$section_id = get_cat_ID('Food and Health');
	$query = new WP_Query( array( 'posts_per_page'=>-1, 'category__and' => array( $cat_id, $section_id ), 'orderby' => 'date', 'order'=>'ASC'  ) );
	//var_dump($section_id);
	/* Start the Loop */
	echo '<div>';
	//usort($query->posts, "comparePostPage"); // sorting the posts according to 'page' custom field
	while ( $query->have_posts() ) 
	{
		$query->the_post();
		get_template_part( 'content', 'pdf' );
	}
	echo '</div>';
	wp_reset_query();
	wp_reset_postdata();
	?>
</div>

<?php // ------ Section Agriculture display-------- ?>
<div id="agriculture">
	<hr class="issue-hr"/>
	<div class="Issue-section-header">
		<a name="Agriculture" ></a>
		<p class="Issue-section-anchors">Agriculture</p>
	</div>
	<hr class="issue-hr"/>
	<?php
	$section_id = get_cat_ID('Agriculture');
	$query = new WP_Query( array( 'posts_per_page'=>-1, 'category__and' => array( $cat_id, $section_id ), 'orderby' => 'date', 'order'=>'ASC' ) );
	/* Start the Loop */
	echo '<div>';
	//usort($query->posts, "comparePostPage"); // sorting the posts according to 'page' custom field
	while ( $query->have_posts() ) 
	{
		$query->the_post();
		get_template_part( 'content', 'pdf' );
	}
	echo '</div>';
	wp_reset_query();
	wp_reset_postdata();
	?>
</div>

<?php // ------ Section Environment display-------- ?>
<div id="environment">
	<hr class="issue-hr"/>
	<div class="Issue-section-header">
		<a name="Environment" ></a>
		<p class="Issue-section-anchors">Environment</p>
	</div>
	<hr class="issue-hr"/>
	<?php		
	$section_id = get_cat_ID('Environment');
	$query = new WP_Query( array( 'posts_per_page'=>-1, 'category__and' => array( $cat_id, $section_id ), 'orderby' => 'date', 'order'=>'ASC' ) );
	/* Start the Loop */
	echo '<div>';
	//usort($query->posts, "comparePostPage"); // sorting the posts according to 'page' custom field
	while ( $query->have_posts() ) 
	{
		$query->the_post();
		get_template_part( 'content', 'pdf' );
	}
	echo '</div>';
	wp_reset_query();
	wp_reset_postdata();
	?>
</div>

<?php // ------ Section Research & News display-------- ?>
<div id="research-and-news" <?php if (!$has_research){ echo 'style="display:none"';} ?>>
	<hr class="issue-hr"/>
	<div class="Issue-section-header" >
		<a name="research-and-news" ></a>
		<p class="Issue-section-anchors">Research & News</p>
	</div>
	<hr class="issue-hr"/>
	
	<?php
	$section_id = get_category_by_slug( 'research-news')->cat_ID;
	$query = new WP_Query( array( 'posts_per_page'=>-1, 'category__and' => array( $cat_id, $section_id ), 'orderby' => 'date', 'order'=>'ASC' ) );
	/* Start the Loop */
	echo '<div>';
	//usort($query->posts, "comparePostPage"); // sorting the posts according to 'page' custom field
	while ( $query->have_posts() ) 
	{
		$query->the_post();
		get_template_part( 'content', 'pdf' );
	}
	echo '</div>';
	wp_reset_query();
	wp_reset_postdata();
	?>
</div>

<!--<hr class="issue-hr"/>-->
<?php 
	
	// Print out the footer section of category Issues
	echo get_reusable_part('Issue footer', '','');
?>
<?php 
else : ?>
	<?php get_template_part( 'content', 'none' ); ?>
<?php 
endif; ?>