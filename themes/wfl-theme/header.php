<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */
?><!DOCTYPE html>
<!--[if lte IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
<!--[if !(lte IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">

	$(document).ready(function(){ 
	
	$(window).scroll(function(){
		if ($(this).scrollTop() > 500) {
			$('.scrollup').fadeIn();
		} else {
			$('.scrollup').fadeOut();
		}
	}); 
	
	$('.scrollup').click(function(){
		$("html, body").animate({ scrollTop: 100 }, 600);
		return false;
	});

});



	$(document).ready(function(){
		if ($("ul.menu > li > ul")){
			$("ul.menu > li > ul").parent('li').addClass("arrow arrow_plus");
		}


		$(".arrow").on('click', function(event){
			
			if($(this).hasClass('arrow_plus')){
				$(this).addClass("arrow_minus").removeClass("arrow_plus");
			}else{
				$(this).addClass("arrow_plus").removeClass("arrow_minus");
			}
			
			$(this).children('ul').slideToggle(300);
			$(this).siblings().children('ul').slideUp(300);
			
			$(this).siblings().children('ul').parent('li').removeClass('arrow_minus').addClass('arrow_plus');
			
		});

	});

	
</script>
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site wrap-page">
	<header id="site-header" class="site-header clearfix">
		
		
			
		<div id="search-box">
			
			<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<input type="text" value="" name="s" id="search_field" placeholder="Search..."/>
				<input type="submit" id="search-submit" value="Go" />
			
			</form>
		</div><!-- search --> 





		<div class = "main-navigation">
			<nav id="navigation">
			<!-- <h3 class="menu-toggle">/*<?php _e( 'Menu', 'wfl_theme' ); ?>*/</h3>
			<a class="assistive-text" href="#content" title="/*<?php esc_attr_e( 'Skip to content', 'wfl_theme' ); ?>"><?php _e( 'Skip to content', 'wfl_theme' ); ?>*/</a> -->
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
			</nav><!-- #site-navigation -->
		</div>
		


		 
		
		
		<div class="logo">
			<a id="logo" rel="home" title="Home" href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<img alt="Home" src="<?php echo get_bloginfo('template_url'); ?>/images/logo.png"/></a>
		</div><!-- logo -->
			
		
		
	

		<?php
		if ( is_home() ) {
			//echo "<style>.main-navigation ul.nav-menu > li.menu-item:first-child > a{ background: none repeat scroll 0% 0% rgb(255, 255, 255) !important; }</style>";
		}
		?>
	</header><!-- #masthead -->
	
	
	<div id="breadcrumb" class="breadcrumb">
	    <?php if(function_exists('bcn_display'))
	    {
	        bcn_display();
	    }?>
	</div>
	<div id="main" class="mainsite clearfix">