<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */
?>
	
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
			<div class="featured-post">
				<?php _e( 'Featured post', 'wfl_theme' ); ?>
			</div>
			<?php endif; ?>
				 
				

			<?php if ( is_search() ) : // Only display Excerpts for Search ?>
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div><!-- .entry-summary -->
			<?php else : ?>
			<div class="display-book clearfix">
	        	<div class="book-thumbnail">
	        		<?php the_post_thumbnail(array(100,150)); ?>
	        	</div>
	        	<div class="book-details">

	                <div class="entry-title-book">
	                    <a class="book-title" href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'wfl_theme' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
	                    	<?php 
	                    		$book_title = get_the_title();

	                    		if(strlen($book_title) >50){
	                    			echo substr($book_title, 0, 50)."&hellip;";
	                    		}else{
	                    			echo get_the_title();
	                    		}
	                    	?>  <!-- shorten book title -->
	                    </a>
	                </div>
					<?php
					$meta = get_post_custom_values('book_editor');
					echo '<p>' . '<strong>Editor:</strong> ' . nl2br($meta[0]) . '</p>';
					$meta = get_post_custom_values('book_price');
					if(!empty($meta[0]))
						echo '<p style="color: #444;">' . '<strong>Cost:</strong> ' . nl2br($meta[0]) . '</p>';
					$meta = get_post_custom_values('book_info');
					if(!empty($meta[0]))
						echo '<p>' . nl2br($meta[0]) . '</p>';
					?>
	                
	                <p align="left" style="color: #660000; font-size: small;"><a href="http://www.world-food.net/pdf/book%20order%20form.pdf"><em>(buy your book!)</em></a></p>



					<div class="entry-meta">
					<?php edit_post_link( __( 'Edit', 'wfl_theme' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-meta -->
				</div> <!--display book-->
			</div><!-- .entry-content -->
			<?php endif; ?>
			
		</article><!-- #post -->