<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */
?>
<style>
.issue_cover_float
{
padding:10px;
width:250px;
float:left;
}
.issue_cover_float_for_authors{
width:33%;
float:left;
font-size:12px;
}
.issue_cover_float_for_partners{
width:50%;
float:left;
}

.author-entry-header {
	margin-top: 1.3em;
	margin-bottom: 1.3em;
}
.entry-content-author {
	overflow : hidden;
	height: auto;
}
</style> <!-- display authors in three columns -->
<issue-article id="<?php the_title(); ?>_post" class="div_no_display" width="100%">
		<header class="entry-header author-entry-header">
			<h1 class="entry-title author-entry-title" id="<?php the_title(); ?>">
				<?php the_title(); ?>
			</h1>
            <!--Viewed:--> <?php // echo do_shortcode('[post_view]'); ?>
		</header><!-- .entry-header -->

		<div class="entry-content entry-content-author" id="<?php the_title(); ?>_content">
			<?php //the_content(); 
				$post = get_post( get_query_var('post') );
				$content = $post->post_content ;
				//var_dump($content);
				$pattern = '/>[ \t\r\n\v\f]*?</';
				$content = preg_replace($pattern, ">\r\n<", $content);
				$array = explode("\r\n", $content);
				//var_dump($array);
				foreach ($array as $line) : ?>
					<div class="issue_cover_float_for_authors" > 
						<?php echo ($line ? $line : ""); ?> 
					</div>
			<?php endforeach; ?>
		</div><!-- .entry-content -->

		<footer class="issue-arti-entry-meta">
			<?php edit_post_link( __( 'Edit', 'wfl_theme' ), '<span class="edit-link">', '</span>' ); ?>
		</footer><!-- .entry-meta -->
		
		
</issue-article><!-- #post -->