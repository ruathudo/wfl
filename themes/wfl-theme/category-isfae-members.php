<?php
/**
 * The template for displaying Category pages.
 *
 * Used to display archive-type pages for posts in a category.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */

get_header(); ?>


<?php

function get_subcat($subcat,$j){
	$out_col ='';

	//var_dump($subcat->cat_name);
	$args = array(
		'posts_per_page'  => -1,
		'category'        => intval($subcat[$j]->cat_ID),
		'orderby'         => 'name',
		'order'           => 'ASC'
		);
	//var_dump($args);
	$posts = get_posts($args);
	//var_dump($posts);
	// Start view:
	$out_col .= '<div id="'. $subcat[$j]->cat_name .'_anchor">';
	//var_dump($subcat->cat_name);
	$out_col .= '<article class="content-by-letter" id="'.$subcat[$j]->cat_name.'"><p id="'.$subcat[$j]->cat_name.'2" class="first_letter">'.$subcat[$j]->cat_name.'</p>';
	$out_col .= '<div class="three_column">';
	foreach ($posts as $post) {
	//var_dump($post);
		$out_col .= '<p style="font-weight:bold; font-size:large; margin-top:0.5em; margin-bottom:0.5em;">'.$post->post_title.'</p>';
		$content = $post->post_content;
		$content = str_replace("\r\n\r\n","<br/>",$content);
		//str_replace("\"","",$content);
		$paragraph_count++;

		$out_col .= '<p id="'.$paragraph_count.'" style="padding-bottom: 10px;"> '.$content.'</p>';
		global $user_ID; 
		if($user_ID and current_user_can('edit_post')) {
			$out_col .= '<a href="'. get_edit_post_link( $post->ID) .'">Edit</a>';
		}
	}
	$out_col .= '</div>'; // display content in three collum
	$out_col .= '</article>';
	$out_col .= '</div>';

	return $out_col;
}

function get_output_from_subcats ($subcats) {
	$paragraph_count=0;
	// create 3 column
	$out_col1 = '';
	$out_col2 = '';
	$out_col2 = '';

	//create total column
	$out_col = '<div  id="content-wrap">';
	$country_number = count($subcats); // count the Letter category ( A, B. C..)

	// Get content from each category
	for ($i=0;$i<$country_number;$i++){
		if($i<$country_number/3){
			$j = $i;
			$out_col1 .= get_subcat($subcats,$j);   // content for column 1
			 
		}else if($i<$country_number - $i + 4){
			$j = $i;
			$out_col2 .= get_subcat($subcats,$j);     // content for column 2
		}else{
			$j = $i;
			$out_col3 .= get_subcat($subcats,$j);     // content for column 3
		}
	}
			
	$out_col .= '<div  class="display-column">';
	$out_col .= $out_col1;
	$out_col .= '</div>'; // .display_column 1

	$out_col .= '<div  class="display-column">';
	$out_col .= $out_col2;
	$out_col .= '</div>'; // .display_column 2

	$out_col .= '<div  class="display-column">';
	$out_col .= $out_col3;
	$out_col .= '</div>'; // .display_column 3


	$out_col .= '</div>'; // content wrap 
	
	
	//echo $line_count = substr_count($out_col, "<br/>") + 1;
	
	return $out_col;
}

// end of function
?>

	<div id="thirdary" class="widget-area-left" role="complementary">
			<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</div><!-- #secondary -->
	<section id="primary" class="site-content">
		<div id="content" role="main">
		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<!--<h1 class="archive-title"><?php //printf( __( 'Category Archives: %s', 'wfl_theme' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>-->

			<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php echo category_description(); ?></div>
			<?php endif; ?>

			
			</header><!-- .archive-header -->



			
		    <hr>
		    <!-- <a style="position: fixed; bottom:5px;right:5px; " href="#" title="Back to Top"><img style="border: none; " src="http://bakerplasticsurgery.com/wp-content/themes/baker/images/TopButton.gif"/></a> -->
			<a href="#" class="scrollup" style="display: inline;">Scroll</a>


			<?php 
				$category = get_category( get_query_var( 'cat' ) );
				$subcats = get_categories( array('parent' => intval($category->cat_ID)));


			?>


			<div class='alphalist'>
				<ul class='hlist'>
				<?php
					
					foreach ($subcats as $subcat) :
						$title = $subcat->cat_name;

						echo "<li><a href='#".$title."' id='".$title."_anchor' onclick=\"showSection('".$title."')\" class='anchor'>".$title."</a></li>";
					endforeach;
					echo "<li><a href='#All' id='All_anchor' onclick=\"showSection('All')\" class='anchor'>All</a></li>";
				?>
				</ul>
			</div>


			<?php 
				
			
				$output .= get_output_from_subcats($subcats);
				echo $output;  // echo output of all html code content
			?>

			
			

		<?php else : ?>
                <?php get_template_part( 'content', 'none' ); ?>
            <?php endif; ?>
		</div><!-- #content -->
	</section><!-- #primary -->



<?php get_sidebar(); ?>
<?php get_footer(); ?>


<script type="text/javascript">
	
/*
	function showSection(foo){

		var anchors = document.getElementsByClassName("anchor");
		for (var i = 0; i < anchors.length; i++) {
			anchors[i].style.color = "blue";
		}  // fill color anchors
		
		 
		var div_content = document.getElementById(foo);                  /// get div include each content has id foo ( A, B, C...)
		var div_first_letter = document.getElementById(foo + '2');     // get div which show first letter which has id A2 B2 C2
		var content_wrap = document.getElementById('content_wrap');
		

		var articles = document.getElementsByTagName("article");
		
			if (foo == "All" ) {
				for ( i = 0; i < articles.length; i++){
					articles[i].className = "div_display content-by-letter";
					div_first_letter = "first_letter";
				
				}
				
			}
			else {
				for ( i = 0; i < articles.length; i++){
					articles[i].className = "div_no_display";
					
				}
			}

		document.getElementById(foo + "_anchor").style.color = "red";	


		if (foo != "All"){
			div_content.className = "div_display content-by-letter";	
			div_first_letter = "first_letter";
		}
				
		
	} */
	
	/*showSection('All');  *//* show and hide Partner List */


	/* scolltop script */

/*
	$(window).load(function(){
  	
  	var anchors = document.getElementsByClassName("anchor");
  	var div_anchor = document.getElementsByClassName("div_anchor");
	
	for(var i=0; i<anchors.length; i++){
		if(i< (anchors.length / 3)){
			$(".div_anchor:eq("+ i + ")").addClass("group1");  // add class group1 to element which has class div_anchor satisfy if()
			var imax1 = i; //return numbers elements of group1 (left)
		}else if(i<(anchors.length - imax1 -2)){
			$(".div_anchor:eq("+ i + ")").addClass("group2");
		}else{
			$(".div_anchor:eq("+ i + ")").addClass("group3");
		}
	}

    $(".group1").wrapAll('<div class="display-column"></div>');
    $(".group2").wrapAll('<div class="display-column"></div>');
    $(".group3").wrapAll('<div class="display-column"></div>');
 	
});
			*/
    

</script>