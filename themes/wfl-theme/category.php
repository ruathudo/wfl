<?php
/**
 * The template for displaying Category pages.
 *
 * Used to display archive-type pages for posts in a category.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */

get_header(); ?>
	<div id="thirdary" class="widget-area-left" role="complementary">
			<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</div><!-- #secondary -->
	
	<section id="primary" class="site-content">
		<div id="content" role="main">
			<?php
			$category = get_category( get_query_var( 'cat' ) );
			
			

			$cat_id = $category->cat_ID;

			$parent_categories = get_ancestors( $cat_id, 'category' );
			$number_of_parents = count( $parent_categories);
			switch ($number_of_parents) {  
				case 0: // Journal page
					get_template_part( 'content', 'years' );
					break;
				case 1:
					// the different format for the years below 2014
					if( (int) $category->name < 2015) {
						get_template_part( 'content', 'issues' );
					} else {
						get_template_part( 'content', 'ejournal' );
					}
					
					break;
				case 2:
					$parent_catname = get_parent_category_name($cat_id,1);
					if( (int) $parent_catname < 2015) {
						get_template_part( 'content', 'articles' );
					} else {
						get_template_part( 'content', 'issues' );
					}	
					break;
				case 3:
					$parent_catname = get_parent_category_name($cat_id,1);
					if( (int) $parent_catname < 2015) {
						get_template_part( 'content', 'articles' );
					} else {
						get_template_part( 'content', 'articles-new' );
					}				
					break;
			}


			?>











		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>