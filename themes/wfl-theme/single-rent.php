<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */

get_header(); ?>

<?php
	function print_house_detail_rows ($house_detail_rows) {
		foreach ($house_detail_rows as $house_detail_row) {
			print_house_detail_row($house_detail_row);
		}
	}
	
	function print_house_detail_row($house_detail_row) { ?>
		<tr class="">
			<td class="house-detail-type">
				<p><?php echo $house_detail_row[0] ?></p>
			</td>
			<td class="">
				<p>
					<?php echo nl2br($house_detail_row[1][0]); 
						if ($house_detail_row[0] === 'Square area for living')
							echo ' m<sup>2</sup>';
					?>
					
				</p>
			</td>
		</tr>
	<?php
	}
	$id_counter =0;
?>
<script>
	var allRentalImages=new Array();
	var rentalImages=new Array();
	var rentalImageSeq=new Array();
	function changeNextImage(c,a){
		var b=getNextImageSeq(c,a);
		setImageUrl(c,getImageUrl(c,b));
		rentalImageSeq[c]=b
	}
	function changePreviousImage(c,a){
		var b=getPreviousImageSeq(c,a);
		setImageUrl(c,getImageUrl(c,b));
		rentalImageSeq[c]=b
	}
	function getNextImageSeq(d,a){
		var c=allRentalImages[d];
		var b=c.length-1;
		if(b<1){
			return null
		}
		if(a>=b){
			return 1
		}
		return a+1
	}
	function getPreviousImageSeq(d,a){
		var c=allRentalImages[d];
		var b=c.length-1;
		if(b<1){
			return null
		}
		if(a<=1){
			return b
		}
		return a-1
	}
	function getImage(c,a){
		var b=allRentalImages[c];
		return b[a]
	}
	function getImageUrl(b,a){return getImage(b,a)
	}
	function setImageUrl(b,a){document.getElementById("image"+b).src=a;
	return
	}
</script>
				
    <div id="thirdary" class="widget-area-left" role="complementary">
        <?php dynamic_sidebar( 'sidebar-2' ); ?>
    </div><!-- #secondary -->
	<div id="primary" class="site-content">
		<div id="content" role="main">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content clearfix">
						<div class="rent-left-column">
							<div> <h1 class="rent-title"><?php echo get_the_title(); ?></h1></div>
							<?php
								$house_detail_rows = array (
									array('Type of the house', get_post_custom_values('house_type')),
									array('Location', get_post_custom_values('house_location')),
									array('Part of the town', get_post_custom_values('house_part_of_town')),
									array('Number of rooms', get_post_custom_values('house_rooms')),
									array('Square area for living', get_post_custom_values('house_area')),
									array('Floor', get_post_custom_values('house_floor')),
									array('Elevator', get_post_custom_values('house_elevator')),
									array('View from the flat', get_post_custom_values('house_view')),
									array('Direction of windows', get_post_custom_values('house_windows_directions')),
									array('Balcony', get_post_custom_values('house_balcony')),
									array('Direction of balcony', get_post_custom_values('house_balcony_direction')),
									array('Energy efficiency classification', get_post_custom_values('house_energy_efficiency')),
									array('Shape of the flat', get_post_custom_values('house_shape')),
									array('Rent cost', get_post_custom_values('house_rent_cost')),
									array('Flat details', get_post_custom_values('house_details'))
								);
								// var_dump ($house_detail_rows);
							?>
							<table id="">
								<tbody>
									<?php print_house_detail_rows ($house_detail_rows); ?>
								</tbody>
							</table>
						</div> <!-- end left column -->
						
						<div class="rent-right-column">
							<script type="text/javascript">
								rentalImages = new Array();
								rentalImageSeq[<?php echo $id_counter;?>] = 1;
								<?php
									$meta = get_post_custom_values('house_images');
									
									if(!empty($meta[0]))
									{
										$images = explode(";", $meta[0]);
										$counter = 1;
										foreach($images as $image_url)
										{
											echo 'var image'. $counter .' = \''. $image_url .'\';';
											echo 'rentalImages['. $counter .'] = image'.$counter.';';
											//echo "</script><img src='". $image_url."' alt='' class='preload' /><script>";

											$counter++;
										}
										$currentImgUrl = $images[0];
									}
									else
									{
										echo 'var image1 = \'default_image_url.jpg\';';
										echo 'rentalImages[1] = image1;';
										$currentImgUrl = 'default_image_url.jpg';
									}
								?>
								allRentalImages[<?php echo $id_counter;?>] = rentalImages;
							</script>
							<div class="rentalListRowDiv" style="text-align:center;">
								<img class="rent-image" id="<?php echo 'image' . $id_counter;?>" src="<?php echo $currentImgUrl; ?>" />
								<a href="" onClick="javascript:changePreviousImage(<?php echo $id_counter;?>,rentalImageSeq[<?php echo $id_counter;?>]); return false;">&larr;</a>
								<a href="" onClick="javascript:changeNextImage(<?php echo $id_counter;?>,rentalImageSeq[<?php echo $id_counter;?>]); return false;">&rarr; </a>
							</div>
							<div class="rent-map">
								<?php
									$rent_map = $post->post_content;
									echo do_shortcode($rent_map);
								?>
							</div>
						</div> <!-- end right column -->
					</div><!-- .entry-content -->
					

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'wfl_theme' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

				<nav class="nav-single">
					<?php //var_dump(get_adjacent_post(true,'',true));?>
				<span class="nav-previous"><?php previous_post_link_same_cat( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'wfl_theme' ) . '</span> Previous' , TRUE, '14 and 15 and 16 and 183'); ?></span>
					<span class="nav-next"><?php next_post_link_same_cat( '%link', 'Next <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'wfl_theme' ) . '</span>',TRUE, '14 and 15 and 16 and 183' ); ?></span>
				</nav><!-- .nav-single -->


		</div><!-- #content -->
	</div><!-- #primary -->

	 
<style>
	img.preload { display: none; } /* preload images */
</style>

<?php //Preload images 
	if(!empty($images))
	{
		foreach($images as $image_url)
		{
			echo "<img src='". $image_url."' alt='' class='preload' /> ";
		}
	}
?>        
	

	
	
<?php get_sidebar(); ?>
<?php get_footer(); ?>