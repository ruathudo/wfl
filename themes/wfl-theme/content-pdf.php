<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Wfl_Theme
 * @since Wfl Theme 1.0
 */
?>
<div class="issue-article-wrapper" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="issue-arti-entry-header">
			<h1 class="issue-arti-entry-title">
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'wfl_theme' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
			</h1>
            <!--Viewed:--> <?php // echo do_shortcode('[post_view]'); ?>
		</header><!-- .entry-header -->

		<div class="issue-arti-entry-content">
			<?php the_excerpt(); ?>
			<?php //echo do_shortcode('[do action="key-word"/]'); // for testing keywords quickly?>
			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'wfl_theme' ), 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->

		<footer class="issue-arti-entry-meta">
			<?php edit_post_link( __( 'Edit', 'wfl_theme' ), '<span class="edit-link">', '</span>' ); ?>
		</footer><!-- .entry-meta -->
		
		
</div><!-- #post -->